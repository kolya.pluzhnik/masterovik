package noty_team.com.masterovik.base.mvp.activity

import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.squareup.otto.Bus
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import noty_team.com.masterovik.ui.activitys.login.LoginActivity.Companion.baseActivity
import noty_team.com.masterovik.utils.convertDpToPixel
import noty_team.com.masterovik.utils.services.navigator.BackNavigator
import noty_team.com.masterovik.utils.services.navigator.Navigator
import noty_team.com.masterovik.utils.services.navigator.managers.ScreenNavigationBackManager
import noty_team.com.masterovik.utils.services.navigator.managers.ScreenNavigationManager
import noty_team.com.masterovik.utils.services.navigator.managers.events.BackPressEvent
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

abstract class BaseActivity : AppCompatActivity() {

    lateinit var context: Context

    lateinit var requests: CompositeDisposable

    lateinit var bus: Bus

    lateinit var navigationBackManager: BackNavigator

    lateinit var mainRouter: Router

    lateinit var navigatorHolder: NavigatorHolder

    var navigator: Navigator = ScreenNavigationManager(this)

    abstract fun layout(): Int

    abstract fun initialization()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context = applicationContext
        requests = CompositeDisposable()
        val cicerone = Cicerone.create()
        mainRouter = cicerone.router
        bus = Bus()
        navigatorHolder = cicerone.navigatorHolder
        navigationBackManager = ScreenNavigationBackManager(this) {
            onBackStackChanged(it)
        }
        bus.register(navigationBackManager)


        if (layout() != 0) {
            setContentView(layout())
            initialization()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    open fun onBackStackChanged(stackSize: Int) {}

    override fun onBackPressed() {
        bus.post(BackPressEvent())
    }

    fun toggleKeyboard(show: Boolean) {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (!show)
            inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        else
            inputMethodManager.toggleSoftInputFromWindow(window.decorView.windowToken, InputMethodManager.SHOW_FORCED, 0)
    }

    fun freeMemory() {
        System.runFinalization()
        Runtime.getRuntime().gc()
        System.gc()
    }

    fun resizeContainerLarge() {

        var lpconteriner = findViewById<View>(R.id.main_fragment_container).layoutParams as ConstraintLayout.LayoutParams
        lpconteriner.setMargins(0, convertDpToPixel(54.0f, this).toInt(), 0, 0)
        findViewById<View>(R.id.main_fragment_container).layoutParams = lpconteriner

        getActionBarView()?.showAB(true)
    }

    fun resizeContainerSmall() {
        getActionBarView()?.showAB(false)
        var lpconteriner = findViewById<View>(R.id.main_fragment_container).layoutParams as ConstraintLayout.LayoutParams
        lpconteriner.setMargins(0, 0, 0, 0)
        findViewById<View>(R.id.main_fragment_container).layoutParams = lpconteriner
    }

    abstract fun getActionBarView(): ActionBarContract.View?
}