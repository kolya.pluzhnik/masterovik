package noty_team.com.masterovik.base.mvp.fragment

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

abstract class BaseFragment<P: BasePresenter>: Fragment() {

    protected var rootView: View? = null
    lateinit var baseActivity: BaseActivity
    var isVisible: (fragment: Fragment) -> Boolean = { true }

    open var presenter: P? = null

    @LayoutRes
    protected abstract fun layout(): Int

    protected abstract fun initialization(view: View, isFirstInit: Boolean)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        baseActivity = (context as BaseActivity)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = if (layout() != 0)
            inflater.inflate(layout(), container, false)
        else
            super.onCreateView(inflater, container, savedInstanceState)
        return if (rootView == null) view else rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initialization(view, rootView == null)
        rootView = view
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        baseActivity.toggleKeyboard(false)
        presenter?.onDestroy()
        super.onDestroy()
    }
}