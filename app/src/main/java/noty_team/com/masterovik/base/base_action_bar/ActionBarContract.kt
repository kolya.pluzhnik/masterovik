package noty_team.com.masterovik.base.base_action_bar

import android.view.ViewGroup
import io.reactivex.Observable

interface ActionBarContract {
    interface View {
        fun showAB(show: Boolean)

        fun showLeftButton(show: Boolean)
        fun setupLeftButton(view: android.view.View)


        fun showRightButton(show: Boolean)
        fun setupRightButton(view: android.view.View)

        fun showCenterText(show: Boolean)
        fun setupCenterText(res: Int)
        fun setupCenterText(string: String)

        fun getAB(): android.view.View
        fun leftButtonAction(): Observable<Any>
        fun rightButtonAction(): Observable<Any>

        fun changeSizeActionBar(boolean: Boolean)

        fun resetView(boolean: Boolean)
        fun getRightContainer(): ViewGroup

        fun setLargeToolbarHeight()
        fun setSmallToolbarHeight()

        fun setLargeContainerWidth()
        fun setSmallContainerWidth()
    }

    interface Presenter {

        fun setupView()
        fun setupActions()
        fun leftButtonAction()
        fun rightButtonAction()
        fun dispose()
        fun start()
        fun stop()
    }
}