package noty_team.com.masterovik.base

import android.app.Application
import io.paperdb.Paper

class AppMasterovik : Application() {
    companion object {
        lateinit var instance: AppMasterovik
            private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Paper.init(applicationContext)
    }
}