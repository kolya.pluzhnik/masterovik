package noty_team.com.masterovik.base.base_action_bar

import android.support.annotation.Nullable
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.toolbar.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.utils.convertDpToPixel
import noty_team.com.masterovik.utils.convertPixelsToDp


class BaseActionBarView(var root: View, override val containerView: View?, var baseActivity: BaseActivity) : LayoutContainer, ActionBarContract.View {

    override fun showAB(isShow: Boolean) {
        root?.visibility = if (isShow) View.VISIBLE else View.GONE
    }

    override fun showLeftButton(isShow: Boolean) {
        left_container.visibility = if (isShow) View.VISIBLE else View.INVISIBLE
        left_container.isEnabled = isShow

    }

    override fun setupLeftButton(view: View) {
        addViewSafe(left_container, view)
    }

    override fun showRightButton(show: Boolean) {
        right_container.visibility = if (show) View.VISIBLE else View.INVISIBLE
        right_container.isEnabled = show
    }


    override fun getRightContainer(): ViewGroup = right_container


    override fun setupRightButton(view: View) {
        addViewSafe(right_container, view)
    }

    override fun showCenterText(show: Boolean) {
        center_text.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun setupCenterText(res: Int) {
        center_text.setText(res)
    }

    override fun setupCenterText(string: String) {
        center_text.text = string
    }

    override fun getAB(): View {
        return containerView!!.findViewById(R.id.root_toolbar)
    }

    override fun leftButtonAction(): Observable<Any> {
        return RxView.clicks(left_container)
    }

    override fun rightButtonAction(): Observable<Any> {
        return RxView.clicks(right_container)
    }


    override fun changeSizeActionBar(boolean: Boolean) {
        if (boolean) {
            left_container.layoutParams.width = 120
            right_container.layoutParams.width = 120

        } else {
            left_container.layoutParams.width = 60
            right_container.layoutParams.width = 60
        }

    }

    override fun setLargeToolbarHeight() {



        var dp = convertDpToPixel(100.0f, containerView?.context)

        toolbar_order.layoutParams.height = dp.toInt()
    }

    override fun setSmallToolbarHeight() {
        /* val density =  as Int*/
        var dp = convertDpToPixel(56.0f, containerView?.context)

        toolbar_order.layoutParams.height = dp.toInt()
    }

    private fun addViewSafe(@Nullable parentNew: ViewGroup?, @Nullable view: View?, @Nullable index: Int?, @Nullable layoutParams: ViewGroup.LayoutParams?) {
        if (parentNew == null || view == null)
            return

        var parent = view.parent as? ViewGroup
        if (parent != null) {
            val transition = parent.layoutTransition

            parent.layoutTransition = null
            parent.removeView(view)
            parent.layoutTransition = transition
        }

        if (index != null) {
            if (layoutParams != null) {
                parentNew.addView(view, index, layoutParams)
            } else {
                parentNew.addView(view, index)
            }
        } else {
            if (layoutParams != null) {
                parentNew.addView(view, layoutParams)
            } else {
                parentNew.addView(view)
            }
        }
    }

    private fun addViewSafe(@Nullable parentNew: ViewGroup?, @Nullable view: View?) {
        parentNew?.let {
            it.removeAllViews()
            parentNew.addView(view)
        }
    }

    override fun resetView(boolean: Boolean) {
        if (boolean) {
            left_container.removeView(left_container.rootView)
        }
    }

    override fun setLargeContainerWidth() {
        var dp = convertDpToPixel(120.0f, containerView?.context)

        left_container.layoutParams.width = dp.toInt()
        right_container.layoutParams.width = dp.toInt()
    }

    override fun setSmallContainerWidth() {
        var dp = convertDpToPixel(60.0f, containerView?.context)
        left_container.layoutParams.width = dp.toInt()
        right_container.layoutParams.width = dp.toInt()
    }
}