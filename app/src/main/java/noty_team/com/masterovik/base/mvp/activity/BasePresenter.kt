package noty_team.com.masterovik.base.mvp.activity

import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

open class BasePresenter: BaseView {

    var disposable = CompositeDisposable()

    override fun onDestroy(){
        disposable.clear()
    }

    fun setClickAction(observable: Observable<Any>, onClick: () -> Unit) {
        disposable.add(observable
                .subscribe({
                    onClick()
                }, {
                    Log.d("error", it.localizedMessage.toString())
                }))
    }
}