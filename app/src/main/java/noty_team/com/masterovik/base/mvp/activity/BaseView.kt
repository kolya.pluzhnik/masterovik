package noty_team.com.masterovik.base.mvp.activity

interface BaseView {
    fun onDestroy()
}