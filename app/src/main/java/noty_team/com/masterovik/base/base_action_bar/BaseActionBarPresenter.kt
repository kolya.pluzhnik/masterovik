package noty_team.com.masterovik.base.base_action_bar

import io.reactivex.disposables.CompositeDisposable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity

class BaseActionBarPresenter(var actionBarView: ActionBarContract.View, var baseActivity: BaseActivity) : ActionBarContract.Presenter {

    lateinit var disposable: CompositeDisposable

    override fun setupView() {

        disposable = CompositeDisposable()
    }

    override fun setupActions() {
        actionBarView.showAB(false)
        actionBarView.leftButtonAction().subscribe { leftButtonAction() }
        actionBarView.rightButtonAction().subscribe { rightButtonAction() }
    }


    override fun leftButtonAction() {


    }

    override fun rightButtonAction() {

    }

    override fun dispose() {
        if (disposable != null)
            disposable.dispose()
    }

    override fun start() {
        setupView()
        setupActions()
    }

    override fun stop() {
        dispose()
    }
}