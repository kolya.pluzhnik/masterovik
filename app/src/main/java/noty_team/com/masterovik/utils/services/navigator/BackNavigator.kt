package noty_team.com.masterovik.utils.services.navigator

interface BackNavigator {
    fun navigateBack(): Unit
    fun tryExitActivity(): Unit
}