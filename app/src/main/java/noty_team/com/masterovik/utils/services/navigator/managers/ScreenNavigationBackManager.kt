package noty_team.com.masterovik.utils.services.navigator.managers

import android.support.v4.app.FragmentManager
import android.util.Log
import com.squareup.otto.Subscribe
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.utils.services.navigator.BackNavigator
import noty_team.com.masterovik.utils.services.navigator.managers.events.BackPressEvent
import noty_team.com.masterovik.utils.services.navigator.managers.events.TryExitActivityEvent
import noty_team.com.masterovik.utils.services.navigator.managers.events.TryNavigateBackEvent


class ScreenNavigationBackManager(var baseActivity: BaseActivity, val onBackStackChanged: (stackSize: Int) -> Unit) : BackNavigator {

    var couldNavigateBack = true

    override fun navigateBack() {
        var fragmentManager = baseActivity.supportFragmentManager

        if (fragmentManager.backStackEntryCount > 1) {
            var backEntry = fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount - 1)
            var fragmentName = backEntry.name

            fragmentManager.popBackStackImmediate(fragmentName, FragmentManager.POP_BACK_STACK_INCLUSIVE)

            onBackStackChanged(fragmentManager.backStackEntryCount)
        } else {
            tryExitActivity()
        }
    }

    override fun tryExitActivity() {
        baseActivity.toggleKeyboard(false)
        exit()
    }

    fun exit() {
        baseActivity.finish()
        baseActivity.freeMemory()
    }

    @Subscribe
    fun onEvent(event: BackPressEvent) {
        if (couldNavigateBack) {
            navigateBack()
        } else {
            baseActivity.bus.post(TryNavigateBackEvent())
        }
    }

    @Subscribe
    fun onEvent(event: TryExitActivityEvent) {
        tryExitActivity()
    }
}