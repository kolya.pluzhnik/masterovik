package noty_team.com.masterovik.utils.services.navigator.managers

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.utils.services.navigator.Navigator
import noty_team.com.masterovik.utils.services.navigator.ScreenAnimType

class ScreenNavigationManager(var activity: BaseActivity) : Navigator {

    override fun navigateToFragment(fragment: Fragment, bundle: Bundle?, addToBackStack: Boolean) {
        if (isSameFragmentAlreadyPlaced(fragment)) {
            return
        }

        val tran = activity.getSupportFragmentManager().beginTransaction()

        if (bundle != null && !bundle.isEmpty) {
            fragment.arguments = bundle
        }

        if (addToBackStack) {
            tran.replace(R.id.main_fragment_container, fragment, fragment.javaClass.getSimpleName())
            tran.addToBackStack(fragment.javaClass.getSimpleName())
        } else {
            tran.replace(R.id.main_fragment_container, fragment)
        }

        tran.commit()

    }

    private fun setAnimationForFragment(animate: ScreenAnimType, tran: FragmentTransaction): FragmentTransaction {

        when (animate) {
            ScreenAnimType.NONE_TYPE -> tran.setCustomAnimations(0, 0)
            ScreenAnimType.FADE_TYPE -> tran.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            ScreenAnimType.BOTTOM_TO_TOP_TYPE -> tran.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
            ScreenAnimType.TOP_TO_BOTTOM_TYPE -> tran.setCustomAnimations(R.anim.slide_out_up, R.anim.slide_in_up)
        }

        return tran

    }


    private fun isSameFragmentAlreadyPlaced(fragment: Fragment): Boolean {
        val existing = activity.getSupportFragmentManager().findFragmentById(R.id.root)
        if (existing != null) {
            if (existing ?.javaClass == fragment) {
                return true
            }
        }
        return false
    }

    companion object {

        val ACTIVITY_REQUEST_CODE = "ScreenNavigationManager.activityRequestCode"
    }
}
