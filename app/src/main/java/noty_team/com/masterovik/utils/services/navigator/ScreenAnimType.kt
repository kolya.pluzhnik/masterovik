package noty_team.com.masterovik.utils.services.navigator

enum class ScreenAnimType {
    NONE_TYPE, FADE_TYPE, RIGHT_TO_LEFT_TYPE, LEFT_TO_RIGHT_TYPE, BOTTOM_TO_TOP_TYPE, TOP_TO_BOTTOM_TYPE
}