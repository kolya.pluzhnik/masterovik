package noty_team.com.masterovik.utils.services.navigator

enum class ScreenType {
    ACTIVITY, FRAGMENT
}