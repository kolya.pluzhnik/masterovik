package noty_team.com.masterovik.utils.services.navigator

import android.os.Bundle
import android.support.v4.app.Fragment

interface Navigator {
    fun navigateToFragment(fragment: Fragment, bundle: Bundle?, addToBackStack: Boolean)
}