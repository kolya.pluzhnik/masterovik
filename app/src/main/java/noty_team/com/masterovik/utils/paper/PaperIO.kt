package noty_team.com.masterovik.utils.paper

import io.paperdb.Paper
import io.paperdb.PaperDbException

class PaperIO {
    companion object {
        private const val USER_ROLE = "USER_ROLE"


        fun setRole(role: Boolean) {
            Paper.book().write(USER_ROLE, role)
        }

        fun getRole(): Boolean {
            return Paper.book().read(USER_ROLE)
        }
    }
}