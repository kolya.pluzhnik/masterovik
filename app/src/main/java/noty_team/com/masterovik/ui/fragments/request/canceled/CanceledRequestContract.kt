package noty_team.com.masterovik.ui.fragments.request.canceled

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface CanceledRequestContract {
    interface View {}
    interface Presenter {
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}