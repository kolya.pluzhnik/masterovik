package noty_team.com.masterovik.ui.fragments.profile_setting.edit_notification

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentEditNotification: BaseFragment<EditNotificationPresenter>() {
    companion object {
        fun newInstance() = FragmentEditNotification()
    }

    override fun layout() = R.layout.fragment_edit_notification

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_edit_notification).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }
}