package noty_team.com.masterovik.ui.fragments.orders.canceled_order

import android.view.View
import kotlinx.android.extensions.LayoutContainer

class CanceledView(override val containerView: View?) : LayoutContainer, CanceledContract.View {
}