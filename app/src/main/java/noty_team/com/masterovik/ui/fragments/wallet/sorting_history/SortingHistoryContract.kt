package noty_team.com.masterovik.ui.fragments.wallet.sorting_history

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface SortingHistoryContract {

    interface View{}
    interface  Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}