package noty_team.com.masterovik.ui.fragments.advert.advert_instance

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentInstanceAdvert : BaseFragment<InstanceAdvertPresenter>() {

    lateinit var instanceAdvertView: InstanceAdvertContract.View
    lateinit var instanceAdvertPresenter: InstanceAdvertContract.Presenter
    companion object {
        fun newInstance(): FragmentInstanceAdvert = FragmentInstanceAdvert()
    }

    override fun layout() = R.layout.fragment_current_advert

    override fun initialization(view: View, isFirstInit: Boolean) {

        instanceAdvertView = InstanceAdvertView()
        instanceAdvertPresenter = InstanceAdvertPresenter()

    }

    override fun onStart() {
        super.onStart()
        instanceAdvertPresenter.start(baseActivity)
    }
}