package noty_team.com.masterovik.ui.fragments.login.terms_agreement

import io.reactivex.Observable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface TermsContract {
    interface View{

        fun continueAction(): Observable<Any>
        fun countinueButtonText(text:String)
    }

    interface  Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}