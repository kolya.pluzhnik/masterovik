package noty_team.com.masterovik.ui.fragments.profile_setting.edit_email

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentEditEmail : BaseFragment<EditEmailPresenter>() {

    companion object {
        fun newInstance() = FragmentEditEmail()
    }
    override fun layout() = R.layout.fragment_edit_email

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_edit_email).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }

    }
}