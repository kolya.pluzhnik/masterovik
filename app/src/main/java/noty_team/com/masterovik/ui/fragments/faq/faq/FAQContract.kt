package noty_team.com.masterovik.ui.fragments.faq.faq

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface FAQContract {

    interface View{}

    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}