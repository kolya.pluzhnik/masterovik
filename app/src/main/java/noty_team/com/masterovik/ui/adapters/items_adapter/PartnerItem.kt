package noty_team.com.masterovik.ui.adapters.items_adapter

data class PartnerItem(var partnerName: String, var date: String, var isActivated: Boolean)