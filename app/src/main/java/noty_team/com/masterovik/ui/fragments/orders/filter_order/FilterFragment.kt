package noty_team.com.masterovik.ui.fragments.orders.filter_order

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FilterFragment : BaseFragment<FilterPresenter>() {
    lateinit var filterPresenter: FilterContract.Presenter
    lateinit var filterView: FilterContract.View

    companion object {
        fun newInstance(): FilterFragment {
            return FilterFragment()
        }
    }

    override fun layout() = R.layout.fragment_filter_order

    override fun initialization(view: View, isFirstInit: Boolean) {

        //  filterView = FilterView(filter_fragment)
        filterPresenter = FilterPresenter(/*filterView*/)

    }

    override fun onStart() {
        filterPresenter.start(baseActivity)
        super.onStart()
    }


}