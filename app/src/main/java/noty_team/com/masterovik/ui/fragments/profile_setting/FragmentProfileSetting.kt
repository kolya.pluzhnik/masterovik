package noty_team.com.masterovik.ui.fragments.profile_setting

import android.view.LayoutInflater
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_email.FragmentEditEmail
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_notification.FragmentEditNotification
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_personal_data.FragmentEditPersonalData
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_phone_number.FragmentEditPhoneNumber
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.FragmentEditSettingService

class FragmentProfileSetting : BaseFragment<ProfileSettingPresenter>() {

    companion object {
        fun newInstance() = FragmentProfileSetting()
    }

    override fun layout() = R.layout.fragment_profile_setting

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.getActionBarView()?.setupCenterText("Настройки")
        baseActivity.getActionBarView()?.setupRightButton(baseActivity.layoutInflater.inflate(R.layout.ab_empty, null))
        baseActivity.getActionBarView()?.setSmallToolbarHeight()
        baseActivity.resizeContainerLarge()





        baseActivity.findViewById<View>(R.id.edit_profile_data).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentEditPersonalData.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }

        baseActivity.findViewById<View>(R.id.edit_phone_number).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentEditPhoneNumber.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }

        baseActivity.findViewById<View>(R.id.edit_email).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentEditEmail.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }

        baseActivity.findViewById<View>(R.id.edit_notification).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentEditNotification.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }
        baseActivity.findViewById<View>(R.id.setting_service).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentEditSettingService.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }


    }
}