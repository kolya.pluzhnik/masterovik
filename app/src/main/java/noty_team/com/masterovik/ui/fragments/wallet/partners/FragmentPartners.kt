package noty_team.com.masterovik.ui.fragments.wallet.partners

import android.view.View
import kotlinx.android.synthetic.main.fragmnet_parthner.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentPartners : BaseFragment<ParthnersPresenter>() {


    lateinit var parthnersView: PartnersContract.View
    lateinit var parthnersPresenter: PartnersContract.Presenter

    override fun layout() = R.layout.fragmnet_parthner

    override fun initialization(view: View, isFirstInit: Boolean) {

        parthnersView = ParthnersView(fragment_partners)
        parthnersPresenter = ParthnersPresenter()


    }

    override fun onStart() {
        super.onStart()
        parthnersPresenter.start(baseActivity)
    }
}