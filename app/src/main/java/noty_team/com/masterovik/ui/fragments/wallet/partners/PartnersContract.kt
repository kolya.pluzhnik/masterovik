package noty_team.com.masterovik.ui.fragments.wallet.partners

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface PartnersContract {
    interface View{}
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}