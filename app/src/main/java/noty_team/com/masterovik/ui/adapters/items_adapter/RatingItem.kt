package noty_team.com.masterovik.ui.adapters.items_adapter

data class RatingItem(var namecustomer: String = "", var date: String = "", var rating: Int = 1)