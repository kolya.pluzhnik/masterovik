package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface EditSettingServiceContract {
    interface View{}
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}