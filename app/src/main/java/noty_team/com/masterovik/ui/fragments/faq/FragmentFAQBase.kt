package noty_team.com.masterovik.ui.fragments.faq

import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_faq_base.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.view_pager.FAQPagerAdapter
import noty_team.com.masterovik.ui.fragments.faq.faq.FragmentFAQ
import noty_team.com.masterovik.ui.fragments.faq.feedback.FragmentFeedback

class FragmentFAQBase : BaseFragment<FAQBasePresenter>() {

    lateinit var faqBaseView: FAQBaseContract.View
    lateinit var faqBasePresenter: FAQBaseContract.Presenter
    /*var page = 0
    var title_tut = ""*/


    companion object {
        fun newInstance(): FragmentFAQBase {
            return FragmentFAQBase()
        }
    }

    override fun layout() = R.layout.fragment_faq_base

    override fun initialization(view: View, isFirstInit: Boolean) {


        faqBaseView = FAQBaseView(fragment_faq_base)
        faqBasePresenter = FAQBasePresenter(faqBaseView)

        initTopBar()

    }

    private fun initTopBar() {

        baseActivity.getActionBarView()?.setLargeToolbarHeight()

        view_pager_faq.adapter = FAQPagerAdapter(childFragmentManager, view_pager_faq, arrayListOf(FragmentFAQ(), FragmentFeedback()))
        tabLayout_faq.setupWithViewPager(view_pager_faq)

        baseActivity.getActionBarView()?.setupCenterText("FAQ")

        var rightView = LayoutInflater.from(baseActivity).inflate(R.layout.ab_empty, null)
        baseActivity.getActionBarView()?.setupRightButton(rightView)

    }

    override fun onStart() {
        super.onStart()
        faqBasePresenter.start(baseActivity)
    }

}