package noty_team.com.masterovik.ui.fragments.wallet

import android.view.View
import kotlinx.android.extensions.LayoutContainer

class BaseWalletView(override val containerView: View?) : LayoutContainer, BaseWalletContract.View {
}