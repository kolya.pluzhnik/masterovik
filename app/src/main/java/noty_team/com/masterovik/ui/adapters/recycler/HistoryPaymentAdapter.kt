package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.ui.adapters.items_adapter.HistoryPaymentItem

class HistoryPaymentAdapter(val items: ArrayList<HistoryPaymentItem>, val context: Context) :
        RecyclerView.Adapter<HistoryPaymentAdapter.ViewHolder>(), View.OnClickListener {

    var baseActivity = context as BaseActivity

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_history_wallet, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        if (items.get(position).isItemParameter) {
            holder.imageCard.setImageResource(R.drawable.ic_023_card)
            holder.parameterCurd.text = "Пополнение баланса"
            holder.curdName.visibility = View.VISIBLE
            holder.amount.text = items.get(position).amount.toString()
            holder.amount.setTextColor(baseActivity.resources.getColor(R.color.colorGreen))

        } else {

            holder.imageCard.setImageResource(R.drawable.ic_022_withdraw)
            holder.parameterCurd.text = "Списание с \nвнутреннего баланса"
            holder.curdName.visibility = View.GONE

            holder.amount.text = "-" + items.get(position).amount.toString()
            holder.amount.setTextColor(baseActivity.resources.getColor(R.color.colorOrange))
        }

        holder.balance.text = "Баланс: " + items.get(position).balance.toString() + "руб."
        holder.date.text = items.get(position).dataPayment


    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var date = view.findViewById<TextView>(R.id.date_history)
        var balance = view.findViewById<TextView>(R.id.balance_history)
        var amount = view.findViewById<TextView>(R.id.sum_history)
        var curdName = view.findViewById<TextView>(R.id.card_name_history)
        var imageCard = view.findViewById<ImageView>(R.id.image_card_history)
        var parameterCurd = view.findViewById<TextView>(R.id.parameter_history)


    }
}