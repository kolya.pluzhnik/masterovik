package noty_team.com.masterovik.ui.fragments.wallet.top_up_balance

import android.view.LayoutInflater
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.wallet.BaseWalletContract

class TopUpBalancePresenter : BasePresenter(), TopUpBalanceContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        intiToolbar()
    }


    override fun setupAction() {

        baseActivity.findViewById<View>(R.id.cross_top_up_balance).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun intiToolbar() {

    }
}