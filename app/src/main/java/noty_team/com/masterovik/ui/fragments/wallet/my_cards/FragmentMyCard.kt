package noty_team.com.masterovik.ui.fragments.wallet.my_cards

import android.view.View
import kotlinx.android.synthetic.main.fragment_my_cards.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentMyCard : BaseFragment<MyCardPresenter>() {

    lateinit var myCurdView: MyCardsContract.View
    lateinit var myCurdPresenter: MyCardsContract.Presenter

    override fun layout() = R.layout.fragment_my_cards

    override fun initialization(view: View, isFirstInit: Boolean) {
        myCurdView = MyCardView(fragment_my_card)
        myCurdPresenter = MyCardPresenter(myCurdView)


    }

    override fun onStart() {
        super.onStart()
        myCurdPresenter.start(baseActivity)
    }
}