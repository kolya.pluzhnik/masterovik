package noty_team.com.masterovik.ui.fragments.faq.feedback

import android.view.View
import kotlinx.android.synthetic.main.fragment_feedback.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.fragments.faq.FragmentFAQBase

class FragmentFeedback : BaseFragment<FeedbackPresenter>() {
    lateinit var feedbackView: FeedbackContract.View
    lateinit var feedbackPresenter: FeedbackContract.Presenter

    companion object {
        fun newInstance(): FragmentFeedback {
            return FragmentFeedback()
        }
    }

    override fun layout() = R.layout.fragment_feedback

    override fun initialization(view: View, isFirstInit: Boolean) {

        feedbackView = FeedbackView(fragment_feedback)
        feedbackPresenter = FeedbackPresenter(feedbackView)


    }

    override fun onStart() {
        super.onStart()
        feedbackPresenter.start(baseActivity)
    }
}