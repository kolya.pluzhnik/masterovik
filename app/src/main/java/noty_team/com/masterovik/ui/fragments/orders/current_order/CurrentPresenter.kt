package noty_team.com.masterovik.ui.fragments.orders.current_order

import android.support.constraint.ConstraintLayout
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.utils.convertDpToPixel

class CurrentPresenter(var currentView: CurrentContract.View) : CurrentContract.Presenter, BasePresenter() {
    lateinit var baseActivity: BaseActivity


    override fun setupView() {

        initToolbar()
    }

    private fun initToolbar() {
        baseActivity.resizeContainerLarge()
    }

    override fun setupAction() {


    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }
}