package noty_team.com.masterovik.ui.fragments.orders.done_order

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_done.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.CurrentOrderItem
import noty_team.com.masterovik.ui.adapters.items_adapter.DoneOrderItem
import noty_team.com.masterovik.ui.adapters.recycler.DoneOrderAdapter

class DoneFragment : BaseFragment<DonePresenter>() {
    lateinit var doneView: DoneContract.View
    lateinit var donePresenter: DoneContract.Presenter

    companion object {
        fun newInstance(): DoneFragment {
            return DoneFragment()
        }
    }

    override fun layout() = R.layout.fragment_done

    override fun initialization(view: View, isFirstInit: Boolean) {
        doneView = DoneView(fragment_done)
        donePresenter = DonePresenter(doneView)


        recycler_done_order.layoutManager = LinearLayoutManager(baseActivity)
        recycler_done_order.adapter = DoneOrderAdapter(createList(), baseActivity)

    }

    private fun createList(): ArrayList<DoneOrderItem> {

        var list: ArrayList<DoneOrderItem> = ArrayList()
        var item1 = DoneOrderItem("Название Заказа", "Имя Фамилия", 15)
        var item2 = DoneOrderItem("Название Заказа", "Имя Фамилия", 22)
        var item3 = DoneOrderItem("Название Заказа", "Имя Фамилия", 18)
        var item4 = DoneOrderItem("Название Заказа", "Имя Фамилия", 2)

        list.add(item1)
        list.add(item2)
        list.add(item3)
        list.add(item4)

        return list
    }

    override fun onStart() {
        super.onStart()
        donePresenter.start(baseActivity)
    }
}