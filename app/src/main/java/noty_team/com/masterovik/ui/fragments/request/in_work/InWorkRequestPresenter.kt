package noty_team.com.masterovik.ui.fragments.request.in_work

import android.support.constraint.ConstraintLayout
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail.FragmentInDetail

class InWorkRequestPresenter : BasePresenter(), InWorkRequestContract.Presenter {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {

    }

    override fun setupAction() {



    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }
}