package noty_team.com.masterovik.ui.fragments.rating.rating

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_rating.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.RatingItem
import noty_team.com.masterovik.ui.adapters.recycler.RatingAdapter

class FragmentRating : BaseFragment<RatingPresenter>() {
    override fun layout() = R.layout.fragment_rating

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_rating.layoutManager = LinearLayoutManager(baseActivity)
        recycler_rating.adapter = RatingAdapter(createList(), baseActivity)

    }

    private fun createList(): ArrayList<RatingItem> {
        var list: ArrayList<RatingItem> = ArrayList()

        var item1 = RatingItem("Имя заказчика", "20 мая 2018", 3)
        var item2 = RatingItem("Имя заказчика", "20 мая 2018", 4)
        var item3 = RatingItem("Имя заказчика", "20 мая 2018", 4)
        var item4 = RatingItem("Имя заказчика", "20 мая 2018", 2)

        list.add(item1)
        list.add(item2)
        list.add(item3)
        list.add(item4)
        list.add(item4)

        return list
    }
}