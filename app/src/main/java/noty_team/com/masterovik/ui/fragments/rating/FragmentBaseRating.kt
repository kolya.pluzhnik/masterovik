package noty_team.com.masterovik.ui.fragments.rating

import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_base_rating.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.view_pager.RatingPageAdapter
import noty_team.com.masterovik.ui.fragments.rating.filter_rating.FragmetFilterRating
import noty_team.com.masterovik.ui.fragments.rating.rating.FragmentRating
import noty_team.com.masterovik.ui.fragments.rating.rating_feedback.FragmentRatingFeedback

class FragmentBaseRating : BaseFragment<BaseRatingPresenter>() {

    companion object {
        fun newInstance() = FragmentBaseRating()
    }

    override fun layout() = R.layout.fragment_base_rating
    override fun initialization(view: View, isFirstInit: Boolean) {


        baseActivity.getActionBarView()?.setSmallContainerWidth()
        view_pager_rating.adapter = RatingPageAdapter(childFragmentManager, view_pager_rating,
                arrayListOf(FragmentRating(), FragmentRatingFeedback()))
        tabLayout_rating.setupWithViewPager(view_pager_rating)


        baseActivity.getActionBarView()?.showAB(true)
        baseActivity.resizeContainerLarge()
        baseActivity.getActionBarView()?.setupCenterText("Мой рейтинг")
        var rightView = LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_filter, null)
        baseActivity.getActionBarView()?.setupRightButton(rightView)

        baseActivity.findViewById<View>(R.id.filter).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmetFilterRating.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }


    }
}