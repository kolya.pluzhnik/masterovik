package noty_team.com.masterovik.ui.activitys.main

import android.content.Intent
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.ab_menu.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarPresenter
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarView
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.ui.activitys.login.LoginActivity.Companion.baseActivity
import noty_team.com.masterovik.ui.fragments.advert.FragmentAdvert
import noty_team.com.masterovik.ui.fragments.faq.FragmentFAQBase
import noty_team.com.masterovik.ui.fragments.login.entrance.EntranceFragment
import noty_team.com.masterovik.ui.fragments.orders.OrderFragment
import noty_team.com.masterovik.ui.fragments.orders.filter_order.FilterFragment
import noty_team.com.masterovik.ui.fragments.profile_setting.FragmentProfileSetting
import noty_team.com.masterovik.ui.fragments.rating.FragmentBaseRating
import noty_team.com.masterovik.ui.fragments.request.FragmentBaseRequest
import noty_team.com.masterovik.ui.fragments.wallet.FragmentBaseWallet

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var actionBarView: ActionBarContract.View
    private lateinit var actionBarPresenter: ActionBarContract.Presenter

    private var acti = CompositeDisposable()


    companion object {
        fun start(baseActivity: BaseActivity) {
            baseActivity.startActivity(Intent(baseActivity, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        }
    }

    override fun layout() = R.layout.activity_main

    override fun initialization() {
        actionBarView = BaseActionBarView(toolbar_order, activity_orders, this)
        actionBarPresenter = BaseActionBarPresenter(actionBarView, this)

        initDrawerButton()

        navigator.navigateToFragment(OrderFragment.newInstance(), null, true)

        navigation_view.setNavigationItemSelectedListener(this)

    }

    override fun onBackStackChanged(stackSize: Int) {
        if (stackSize == 1) {
            initDrawerButton()
        }
    }

    fun initDrawerButton() {
        var leftView = LayoutInflater.from(this).inflate(R.layout.ab_menu, null)
        actionBarView.setupLeftButton(leftView)

        setClickAction(RxView.clicks(leftView.findViewById<View>(R.id.mmm))) {
            activity_orders.openDrawer(Gravity.START)
        }

    }

    fun setClickAction(observable: Observable<Any>, onClick: () -> Unit) {
        acti.add(observable
                .subscribe({
                    onClick()
                }, {
                    Log.d("error", it.localizedMessage.toString())
                }))
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0?.itemId) {
            R.id.nav_order -> {
                navigator.navigateToFragment(OrderFragment.newInstance(), null, true)
            }
            R.id.nav_advert -> {
                navigator.navigateToFragment(FragmentAdvert.newInstance(), null, true)
            }
            R.id.nav_request -> {
                navigator.navigateToFragment(FragmentBaseRequest.newInstance(), null, true)
            }
            R.id.nav_rating -> {
                navigator.navigateToFragment(FragmentBaseRating.newInstance(), null, true)
            }
            R.id.nav_packet -> {
                navigator.navigateToFragment(FragmentBaseWallet.newInstance(), null, true)
            }
            R.id.nav_profile -> {
                navigator.navigateToFragment(FragmentProfileSetting.newInstance(), null, true)
            }
            R.id.item_order_services -> {

            }
            R.id.item_faq -> {
                navigator.navigateToFragment(FragmentFAQBase.newInstance(), null, true)
            }
            R.id.item_exit -> {
                Log.d("TAG", "Test")
            }
        }

        activity_orders.closeDrawer(Gravity.START)
        return true
    }


    override fun getActionBarView(): ActionBarContract.View? = actionBarView
}