package noty_team.com.masterovik.ui.fragments.wallet

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface BaseWalletContract {
    interface View{

    }
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}