package noty_team.com.masterovik.ui.fragments.faq.faq

import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

class FAQPresenter(var faqView: FAQContract.View) : BasePresenter(), FAQContract.Presenter {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {

    }

    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }
}