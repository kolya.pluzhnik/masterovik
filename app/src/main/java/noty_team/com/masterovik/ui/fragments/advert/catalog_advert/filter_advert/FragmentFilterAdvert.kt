package noty_team.com.masterovik.ui.fragments.advert.catalog_advert.filter_advert

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentFilterAdvert : BaseFragment<FilterAdvertPresenter>() {
    lateinit var filterAdvertView: FilterAdvertContract.View
    lateinit var filterAdvertPresenter: FilterAdvertContract.Presenter

    companion object {
        fun newInstance(): FragmentFilterAdvert = FragmentFilterAdvert()
    }

    override fun layout() = R.layout.fragment_filter_adverts

    override fun initialization(view: View, isFirstInit: Boolean) {

        filterAdvertView = FilterAdvertView()
        filterAdvertPresenter = FilterAdvertPresenter(filterAdvertView)


    }

    override fun onStart() {
        super.onStart()
        filterAdvertPresenter.start(baseActivity)
    }
}