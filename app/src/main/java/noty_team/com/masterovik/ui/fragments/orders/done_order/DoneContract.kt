package noty_team.com.masterovik.ui.fragments.orders.done_order

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface DoneContract {
    interface View{}
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}