package noty_team.com.masterovik.ui.fragments.wallet.sorting_history

import android.view.LayoutInflater
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.wallet.BaseWalletContract

class SortingHistoryPresenter(var sortingHistoryView: SortingHistoryContract.View) : BasePresenter(), SortingHistoryContract.Presenter {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
    }


    override fun setupAction() {

        baseActivity.findViewById<View>(R.id.cross_sorting_history).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
            baseActivity.resizeContainerLarge()
        }

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initToolbar() {


    }
}