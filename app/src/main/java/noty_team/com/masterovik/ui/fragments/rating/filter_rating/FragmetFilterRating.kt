package noty_team.com.masterovik.ui.fragments.rating.filter_rating

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmetFilterRating : BaseFragment<FilterRatingPresenter>() {

    companion object {
        fun newInstance() = FragmetFilterRating()
    }


    override fun layout() = R.layout.fragment_filter_rating

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_filter_rating).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }
}