package noty_team.com.masterovik.ui.fragments.advert.catalog_advert

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface CatalogAdvertContract {
    interface View
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}