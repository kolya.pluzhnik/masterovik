package noty_team.com.masterovik.ui.adapters.items_adapter

data class FAQItem(var title: String, var subtitle: String)