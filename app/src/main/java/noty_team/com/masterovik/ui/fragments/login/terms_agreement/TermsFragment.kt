package noty_team.com.masterovik.ui.fragments.login.terms_agreement

import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.fragment_terms_agreement.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class TermsFragment : BaseFragment<TermsPresenter>() {

    lateinit var termsView: TermsContract.View
    lateinit var termsPresenter: TermsContract.Presenter

    companion object {
        fun newInstance(): TermsFragment {
            return TermsFragment()
        }
    }

    override fun layout() = R.layout.fragment_terms_agreement

    override fun initialization(view: View, isFirstInit: Boolean) {

        termsView = TermsView(fragment_terms_agreement)
        termsPresenter = TermsPresenter(termsView)
    }

    override fun onStart() {
        super.onStart()
        termsPresenter.start(baseActivity)
    }

    override fun onStop() {
        super.onStop()
        termsPresenter.stop()
    }
}
