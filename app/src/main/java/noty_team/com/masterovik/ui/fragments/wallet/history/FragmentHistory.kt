package noty_team.com.masterovik.ui.fragments.wallet.history

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_histoty.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.HistoryPaymentItem
import noty_team.com.masterovik.ui.adapters.recycler.HistoryPaymentAdapter

class FragmentHistory : BaseFragment<HistoryPresenter>() {

    lateinit var historyView: HistotyContract.View
    lateinit var historyPresenter: HistotyContract.Presenter

    override fun layout() = R.layout.fragment_histoty

    override fun initialization(view: View, isFirstInit: Boolean) {


        //  historyView = HistoryView(fragment_history)
        historyPresenter = HistoryPresenter()

        recycler_history.layoutManager = LinearLayoutManager(baseActivity)
        recycler_history.adapter = HistoryPaymentAdapter(createList(), baseActivity)


    }

    private fun createList(): ArrayList<HistoryPaymentItem> {
        var list: ArrayList<HistoryPaymentItem> = ArrayList()
        var item = HistoryPaymentItem(true, "10 февраля 2019", 4000, 5000)
        var item2 = HistoryPaymentItem(false, "10 февраля 2019", 3000, 7000)
        var item3 = HistoryPaymentItem(false, "10 февраля 2019", 3000, 7000)
        var item4 = HistoryPaymentItem(true, "10 февраля 2019", 3040, 8000)
        var item5 = HistoryPaymentItem(false, "10 февраля 2019", 3000, 2000)
        var item6 = HistoryPaymentItem(false, "10 февраля 2019", 3100, 7000)
        var item7 = HistoryPaymentItem(true, "10 февраля 2019", 1000, 7060)
        var item8 = HistoryPaymentItem(false, "10 февраля 2019", 3000, 7000)

        list.add(item)
        list.add(item2)
        list.add(item3)
        list.add(item4)
        list.add(item5)
        list.add(item6)
        list.add(item7)
        list.add(item8)

        return list
    }

    override fun onStart() {
        super.onStart()
        historyPresenter.start(baseActivity)
    }
}