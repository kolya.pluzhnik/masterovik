package noty_team.com.masterovik.ui.fragments.orders.current_order

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_current.*

import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.CurrentOrderItem
import noty_team.com.masterovik.ui.adapters.items_adapter.FAQItem
import noty_team.com.masterovik.ui.adapters.recycler.CurrentOrderAdapter

class CurrentFragment : BaseFragment<CurrentPresenter>() {

    lateinit var currentView: CurrentContract.View
    lateinit var currentPresenter: CurrentContract.Presenter

    companion object {
        fun newInstance(): CurrentFragment {
            return CurrentFragment()
        }
    }

    override fun layout() = R.layout.fragment_current

    override fun initialization(view: View, isFirstInit: Boolean) {

        currentView = CurrentView(fragment_current)
        currentPresenter = CurrentPresenter(currentView)



        recycler_current_order.layoutManager = LinearLayoutManager(baseActivity)
        recycler_current_order.adapter = CurrentOrderAdapter(createList(), baseActivity)
    }

    private fun createList(): ArrayList<CurrentOrderItem> {
        var list: ArrayList<CurrentOrderItem> = ArrayList()
        var item1 = CurrentOrderItem("Название Заказа", "Имя Фамилия", 15)
        var item2 = CurrentOrderItem("Название Заказа", "Имя Фамилия", 22)
        var item3 = CurrentOrderItem("Название Заказа", "Имя Фамилия", 6)

        list.add(item1)
        list.add(item2)
        list.add(item3)

        return list
    }

    override fun onStart() {
        super.onStart()
        currentPresenter.start(baseActivity)
    }
}