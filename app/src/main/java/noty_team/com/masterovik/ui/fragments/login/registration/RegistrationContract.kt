package noty_team.com.masterovik.ui.fragments.login.registration

import io.reactivex.Observable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface RegistrationContract {
    interface View {
        fun continueAction(): Observable<Any>


        fun showGiveService(show: Boolean)

        fun setButtonText(text: String)

        fun inviteButtonAction(): Observable<Any>

        fun showSuccessInfo(show: Boolean)

        fun showDialoginvite(show: Boolean)
    }

    interface Presenter {

        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}