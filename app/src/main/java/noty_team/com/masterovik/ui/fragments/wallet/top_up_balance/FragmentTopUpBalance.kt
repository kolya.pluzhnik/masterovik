package noty_team.com.masterovik.ui.fragments.wallet.top_up_balance

import android.view.View
import kotlinx.android.synthetic.main.fragment_top_up_balance.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentTopUpBalance : BaseFragment<TopUpBalancePresenter>() {

    lateinit var topUpBalanceView: TopUpBalanceContract.View
    lateinit var topUpBalancePresenter: TopUpBalanceContract.Presenter

    companion object {
        fun newInstance(): FragmentTopUpBalance {
            return FragmentTopUpBalance()
        }
    }

    override fun layout() = R.layout.fragment_top_up_balance

    override fun initialization(view: View, isFirstInit: Boolean) {

        topUpBalanceView = TopUpBalanceView(fragment_top_up_balance)
        topUpBalancePresenter = TopUpBalancePresenter()

    }

    override fun onStart() {
        super.onStart()
        topUpBalancePresenter.start(baseActivity)
    }

    override fun onDestroy() {
        super.onDestroy()
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
    }
}