package noty_team.com.masterovik.ui.fragments.orders.filter_order

import android.view.LayoutInflater
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

class FilterPresenter(/*var filterContract: FilterContract.View*/) : FilterContract.Presenter, BasePresenter() {

    lateinit var baseActivity: BaseActivity


    override fun setupView() {
        initActionBar()
    }


    override fun setupAction() {

        baseActivity.findViewById<View>(R.id.cross_filter_order).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
    }


    private fun initActionBar() {

    }
}