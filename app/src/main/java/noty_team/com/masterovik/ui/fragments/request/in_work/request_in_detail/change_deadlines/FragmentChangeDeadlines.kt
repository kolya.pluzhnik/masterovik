package noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail.change_deadlines

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentChangeDeadlines : BaseFragment<ChangeDeadlinesPresenter>() {
    companion object {
        fun newInstance() = FragmentChangeDeadlines()
    }

    override fun layout() = R.layout.fragment_change_deadline

    override fun initialization(view: View, isFirstInit: Boolean) {


        baseActivity.findViewById<View>(R.id.cross_change_deadline).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }

    }
}