package noty_team.com.masterovik.ui.fragments.login.selection_role

import android.view.View
import kotlinx.android.synthetic.main.fragment_selectino_role.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class SelectionRoleFragment: BaseFragment<SelectionRolePresenter>() {

    lateinit var selectionRoleView: SelectionRoleContract.View
    lateinit var selectionRolePresenter: SelectionRoleContract.Presenter

    companion object {
        fun newInstance(): SelectionRoleFragment {
            return SelectionRoleFragment()
        }
    }

    override fun layout() = R.layout.fragment_selectino_role

    override fun initialization(view: View, isFirstInit: Boolean) {

        selectionRoleView = SelectionRoleView(fragment_select_role)
        selectionRolePresenter = SelectionRolePresenter(selectionRoleView)
    }

    override fun onStart() {
        super.onStart()
        selectionRolePresenter.start(baseActivity)
    }

    override fun onStop() {
        super.onStop()
        selectionRolePresenter.stop()
    }

}