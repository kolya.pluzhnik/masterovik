package noty_team.com.masterovik.ui.fragments.login.registration

import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.button_continue.*
import kotlinx.android.synthetic.main.button_continue.view.*
import kotlinx.android.synthetic.main.fragment_registration_form.*
import noty_team.com.masterovik.R

class RegistrationView(override val containerView: View?) : LayoutContainer, RegistrationContract.View {

    override fun continueAction(): Observable<Any> {
        return RxView.clicks(continue_btn)
    }

    override fun showGiveService(show: Boolean) {
        if (show) {
            ll_company_info.visibility = View.VISIBLE
            invite_code.visibility = View.VISIBLE
        } else {
            ll_company_info.visibility = View.GONE
            invite_code.visibility = View.GONE
        }
    }

    override fun setButtonText(text: String) {
        continue_btn.text = text
    }

    override fun inviteButtonAction(): Observable<Any> {
        return RxView.clicks(invite_code_button)
    }

    override fun showSuccessInfo(show: Boolean) {
        if (show) {
            success_info.visibility = View.VISIBLE
        } else {
            success_info.visibility = View.GONE
        }
    }

    override fun showDialoginvite(show: Boolean) {
        if (show) {
            val mDialogView = LayoutInflater.from(containerView?.context).inflate(R.layout.dialog_user_invite, null)

            val mBuilder = AlertDialog.Builder(containerView!!.context)
                    .setView(mDialogView)


            val mAlertDialog = mBuilder.show()

            mDialogView.continue_btn.setOnClickListener {
                mAlertDialog.dismiss()

                showSuccessInfo(true)
            }
        }

    }
}