package noty_team.com.masterovik.ui.fragments.request

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface BaseRequestContract {
    interface View{}
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}