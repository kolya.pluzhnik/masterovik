package noty_team.com.masterovik.ui.fragments.login.registration

import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import io.reactivex.Observable
import kotlinx.android.synthetic.main.button_continue.view.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.login.terms_agreement.TermsFragment
import noty_team.com.masterovik.utils.paper.PaperIO

class RegistrationPresenter(var registrationView: RegistrationContract.View) : RegistrationContract.Presenter, BasePresenter() {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        if (PaperIO.getRole() == true) {
            setupUseService()
        } else {
            setupGiveService()
        }

        registrationView.setButtonText("Продолжить")
        initActionBar()
    }

    //give service
    private fun setupGiveService() {
        registrationView.showGiveService(true)
    }

    //use service
    private fun setupUseService() {
        registrationView.showGiveService(false)
    }

    override fun setupAction() {

        if (PaperIO.getRole() == true) {
            setupUseServiceAction()
        } else {
            setupGiveServiceAction()
        }

        setClickAction(registrationView.continueAction()) {
            baseActivity.navigator.navigateToFragment(TermsFragment.newInstance(), null, true)
        }
    }

    private fun setupGiveServiceAction() {
        setClickAction(registrationView.inviteButtonAction()) {
            registrationView.showDialoginvite(true)
        }
    }

    private fun setupUseServiceAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
        disposable.clear()
        disposable.dispose()
    }


    private fun initActionBar() {
        baseActivity.getActionBarView()?.showLeftButton(false)
    }
}