package noty_team.com.masterovik.ui.fragments.login.entrance

import android.view.View
import kotlinx.android.synthetic.main.fragment_entrance.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class EntranceFragment : BaseFragment<EntrancePresenter>() {

    lateinit var entranceView: EntranceContract.View
//    lateinit var entrancePresenter: EntranceContract.Presenter

    companion object {
        fun newInstance(): EntranceFragment {
            return EntranceFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_entrance

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.getActionBarView()?.showAB(false)

        entranceView = EntranceView(fragment_entrance)
        presenter = EntrancePresenter(entranceView)

    }

    override fun onStart() {
        super.onStart()
        presenter?.start(baseActivity)
    }

    override fun onStop() {
        super.onStop()
        presenter?.stop()
    }
}