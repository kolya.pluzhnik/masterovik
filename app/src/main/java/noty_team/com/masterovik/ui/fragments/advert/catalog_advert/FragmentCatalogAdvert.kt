package noty_team.com.masterovik.ui.fragments.advert.catalog_advert

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentCatalogAdvert : BaseFragment<CatalogAdvertPresenter>() {

    lateinit var catalogAdvertView: CatalogAdvertContract.View
    lateinit var catalogAdvertPresenter: CatalogAdvertContract.Presenter

    override fun layout() = R.layout.fragment_catalog_advert

    override fun initialization(view: View, isFirstInit: Boolean) {

        catalogAdvertView = CatalogAdvertView()
        catalogAdvertPresenter = CatalogAdvertPresenter()
    }

    override fun onStart() {
        super.onStart()
        catalogAdvertPresenter.start(baseActivity)
    }
}