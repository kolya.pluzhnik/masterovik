package noty_team.com.masterovik.ui.fragments.profile_setting.edit_personal_data

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentEditPersonalData : BaseFragment<EditPersonalDataPresenter>() {

    companion object {
        fun newInstance() = FragmentEditPersonalData()
    }
    override fun layout() = R.layout.fragment_edit_personal_data

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.findViewById<View>(R.id.cross_edit_personal_data).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }
}