package noty_team.com.masterovik.ui.fragments.wallet.my_cards

import io.reactivex.Observable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface MyCardsContract {
    interface View{
        fun topUpBalanceAction():Observable<Any>
    }
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}