package noty_team.com.masterovik.ui.fragments.advert.my_advert

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentMyAdvert : BaseFragment<MyAdvertPresenter>() {

    lateinit var myAdvertPresenter: MyAdvertContract.Presenter
    lateinit var myAdvertView: MyAdvertContract.View

    override fun layout() = R.layout.fragment_my_adver

    override fun initialization(view: View, isFirstInit: Boolean) {
        myAdvertView = MyAdvertView()
        myAdvertPresenter = MyAdvertPresenter()
    }

    override fun onStart() {
        super.onStart()
        myAdvertPresenter.start(baseActivity)
    }

}