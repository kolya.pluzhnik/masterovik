package noty_team.com.masterovik.ui.fragments.orders.current_order.order

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.fragments.orders.complaint.FragmentComplaint
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_amount_payment.FragmentChangeAmountPayment
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_date_order.FragmentChangeDateOrder
import noty_team.com.masterovik.ui.fragments.orders.profile_order.FragmentProfileOrder

class FragmentOrderItem : BaseFragment<OrderItemPresenter>() {

    companion object {
        fun newInstance() = FragmentOrderItem()
    }

    override fun layout() = R.layout.fragment_order_item

    override fun initialization(view: View, isFirstInit: Boolean) {


        baseActivity.findViewById<View>(R.id.cross_current_item).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
        baseActivity.findViewById<View>(R.id.leave_complaint_current).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentComplaint.newInstance(), null, true)
        }

        baseActivity.findViewById<View>(R.id.change_date_order).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentChangeDateOrder.newInstance(), null, true)
        }
        baseActivity.findViewById<View>(R.id.change_amount_payment).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentChangeAmountPayment.newInstance(), null, true)
        }

        baseActivity.findViewById<View>(R.id.user_name).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentProfileOrder.newInstance(), null, true)
        }

    }
}