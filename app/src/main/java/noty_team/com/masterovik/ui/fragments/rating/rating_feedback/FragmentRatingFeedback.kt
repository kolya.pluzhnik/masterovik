package noty_team.com.masterovik.ui.fragments.rating.rating_feedback

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_rating_feedbeack.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.FeedbackRatingItem
import noty_team.com.masterovik.ui.adapters.recycler.FeedbackRatingAdapter

class FragmentRatingFeedback : BaseFragment<RatingFeedbackPresenter>() {
    override fun layout() = R.layout.fragment_rating_feedbeack

    override fun initialization(view: View, isFirstInit: Boolean) {

        recycler_rating_feedback.layoutManager = LinearLayoutManager(baseActivity)
        recycler_rating_feedback.adapter = FeedbackRatingAdapter(createList(), baseActivity)
    }

    private fun createList(): ArrayList<FeedbackRatingItem> {
        var list: ArrayList<FeedbackRatingItem> = ArrayList()

        var item = FeedbackRatingItem("Имя заказчика", "20 мая 2018", 4, getString(R.string.feedback_setting))

        list.add(item)
        list.add(item)
        list.add(item)
        list.add(item)

        return list
    }
}