package noty_team.com.masterovik.ui.activitys.splash

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.ui.activitys.login.LoginActivity
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import java.util.concurrent.TimeUnit

class SplashActivity : BaseActivity() {

    override fun layout(): Int = R.layout.activity_splash

    override fun initialization() {

        startWaitTimer()
    }

    private fun startWaitTimer() {
        requests.add(Observable
                .timer(2000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {}, ::openActivity))
    }

    private fun openActivity() {
        LoginActivity.start(this)
        finish()
    }

    override fun getActionBarView(): ActionBarContract.View? = null

}