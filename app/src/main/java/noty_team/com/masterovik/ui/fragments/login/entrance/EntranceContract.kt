package noty_team.com.masterovik.ui.fragments.login.entrance

import io.reactivex.Observable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BaseView


interface EntranceContract {
    interface View {
        fun showSMSCode(show: Boolean)

        fun getCodeAction(): Observable<Any>

        fun stateEnabled(stateEnabled: Boolean)

        fun continueAction(): Observable<Any>


        fun setTime(timer: String)

        fun timerVisibility(show: Boolean)


    }

    interface Presenter: BaseView {
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()

    }
}