package noty_team.com.masterovik.ui.fragments.rating.rating_review_detail

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentReviewDetail : BaseFragment<ReviewDetailPresenter>() {

    companion object {
        fun newInstance() = FragmentReviewDetail()
    }

    override fun layout() = R.layout.fragment_review_detail

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_rating).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }

    }
}