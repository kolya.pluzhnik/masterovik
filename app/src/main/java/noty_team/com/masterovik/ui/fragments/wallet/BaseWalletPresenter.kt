package noty_team.com.masterovik.ui.fragments.wallet

import android.view.LayoutInflater
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

class BaseWalletPresenter : BasePresenter(), BaseWalletContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
    }


    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initToolbar() {
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        baseActivity.getActionBarView()?.setupCenterText("Платежные карты")
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_empty, null))
        baseActivity.resizeContainerLarge()
    }
}