package noty_team.com.masterovik.ui.fragments.orders

import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import com.notyteam.aist.android.ui.adapters.view_pager.OrderPagerAdapter
import kotlinx.android.synthetic.main.fragment_current_orders.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.fragments.orders.canceled_order.CanceledFragment
import noty_team.com.masterovik.ui.fragments.orders.current_order.CurrentFragment
import noty_team.com.masterovik.ui.fragments.orders.done_order.DoneFragment
import noty_team.com.masterovik.utils.convertDpToPixel

class OrderFragment : BaseFragment<OrderPresenter>() {

    lateinit var orderPresenter: OrderContract.Presenter
    lateinit var orderView: OrderContract.View

    companion object {
        fun newInstance(): OrderFragment {
            return OrderFragment()
        }
    }


    override fun layout() = R.layout.fragment_current_orders

    override fun initialization(view: View, isFirstInit: Boolean) {

        initTopBar()
        orderView = OrderView(fragment_current_order)
        orderPresenter = OrderPresenter()


        baseActivity.getActionBarView()?.setLargeToolbarHeight()


    }

    private fun initTopBar() {


        baseActivity.resizeContainerLarge()
        baseActivity.getActionBarView()?.showAB(true)


        var rightView = LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_menu, null)
        baseActivity.getActionBarView()?.setupRightButton(rightView)
        baseActivity.getActionBarView()?.setLargeContainerWidth()

        view_pager_order.adapter = OrderPagerAdapter(childFragmentManager, view_pager_order, arrayListOf(CurrentFragment(), DoneFragment(), CanceledFragment()))
        tabLayout_order.setupWithViewPager(view_pager_order)

    }

    override fun onStart() {
        super.onStart()
        orderPresenter.start(baseActivity)
    }
}
