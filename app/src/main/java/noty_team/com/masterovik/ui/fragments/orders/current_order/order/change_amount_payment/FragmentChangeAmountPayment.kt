package noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_amount_payment

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentChangeAmountPayment : BaseFragment<ChangePaymountPresenter>() {

    companion object {
        fun newInstance() = FragmentChangeAmountPayment()
    }
    override fun layout() = R.layout.fragment_change_amount_payment

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_change_amount_payment).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }

}