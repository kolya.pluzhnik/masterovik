package noty_team.com.masterovik.ui.fragments.request

import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.orders.filter_order.FilterFragment
import noty_team.com.masterovik.utils.convertDpToPixel

class BaseRequestPresenter : BasePresenter(), BaseRequestContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
    }

    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initToolbar() {
        baseActivity.getActionBarView()?.showAB(true)
        baseActivity.getActionBarView()?.setupCenterText("Заявки")

        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.resizeContainerLarge()

        baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_filter, null))


        baseActivity.findViewById<View>(R.id.filter).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FilterFragment.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }
    }
}