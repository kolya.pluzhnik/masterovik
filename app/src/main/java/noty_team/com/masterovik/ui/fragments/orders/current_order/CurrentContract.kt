package noty_team.com.masterovik.ui.fragments.orders.current_order

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface CurrentContract {
    interface View{}
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}