package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_current_order.view.*
import kotlinx.android.synthetic.main.item_done_order.view.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.ui.adapters.items_adapter.DoneOrderItem
import noty_team.com.masterovik.ui.fragments.orders.done_order.order.FragmentDoneItem

class DoneOrderAdapter(val items: ArrayList<DoneOrderItem>, val context: Context) :
        RecyclerView.Adapter<DoneOrderAdapter.ViewHolder>(), View.OnClickListener {

    var baseActivity = context as BaseActivity

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_done_order, parent, false), baseActivity)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.nameCurrentOrder?.text = items.get(position).nameOrder
        holder?.nameSurnameCurrenrOrder?.text = items.get(position).nameSurname
        holder?.currentOrderDay?.text = items.get(position).date.toString() + " января 2019"

        holder?.doneItemContainerbtn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.done_item_container -> {

                baseActivity.navigator.navigateToFragment(FragmentDoneItem.newInstance(), null, true)

                baseActivity.getActionBarView()?.showAB(false)
                var lpconteriner = baseActivity.findViewById<View>(R.id.main_fragment_container).layoutParams as ConstraintLayout.LayoutParams
                lpconteriner.setMargins(0, 0, 0, 0)
                baseActivity.findViewById<View>(R.id.main_fragment_container).layoutParams = lpconteriner

            }
        }
    }

    class ViewHolder(view: View, var baseActivity: BaseActivity) : RecyclerView.ViewHolder(view) {

        var nameCurrentOrder = view.name_done_order
        var nameSurnameCurrenrOrder = view.name_surname_done_order
        var currentOrderDay = view.date_done_order_done
        var doneItemContainerbtn = view.done_item_container

    }
}