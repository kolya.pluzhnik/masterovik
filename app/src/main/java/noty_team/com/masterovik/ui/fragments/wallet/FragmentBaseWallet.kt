package noty_team.com.masterovik.ui.fragments.wallet

import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.fragment_base_wallet.*
import kotlinx.android.synthetic.main.fragment_current_orders.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.view_pager.WalletPageAdapter
import noty_team.com.masterovik.ui.fragments.orders.OrderFragment
import noty_team.com.masterovik.ui.fragments.wallet.history.FragmentHistory
import noty_team.com.masterovik.ui.fragments.wallet.my_cards.FragmentMyCard
import noty_team.com.masterovik.ui.fragments.wallet.partners.FragmentPartners
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.FragmentSortingHistory

class FragmentBaseWallet : BaseFragment<BaseWalletPresenter>() {

    lateinit var baseWalletView: BaseWalletContract.View
    lateinit var baseWalletPresenter: BaseWalletContract.Presenter


    companion object {
        fun newInstance(): FragmentBaseWallet {
            return FragmentBaseWallet()
        }
    }

    override fun layout() = R.layout.fragment_base_wallet

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseWalletView = BaseWalletView(fragment_base_wallet)
        baseWalletPresenter = BaseWalletPresenter()

        view_pager_wallet.adapter = WalletPageAdapter(childFragmentManager, view_pager_wallet,
                arrayListOf(FragmentMyCard(), FragmentHistory(), FragmentPartners()))
        tabLayout_wallet.setupWithViewPager(view_pager_wallet)

        tabLayout_wallet.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 1) {
                    baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_filter, null))
                    baseActivity.findViewById<View>(R.id.right_container).setOnClickListener {
                        baseActivity.navigator.navigateToFragment(FragmentSortingHistory.newInstance(), null, true)

                        baseActivity.resizeContainerSmall()
                        baseActivity.getActionBarView()?.setLargeToolbarHeight()
                    }
                } else {
                    baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_empty, null))
                    baseActivity.findViewById<View>(R.id.right_container).setOnClickListener {
                    }
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


    }

    override fun onStart() {
        super.onStart()
        baseWalletPresenter.start(baseActivity)
    }
}