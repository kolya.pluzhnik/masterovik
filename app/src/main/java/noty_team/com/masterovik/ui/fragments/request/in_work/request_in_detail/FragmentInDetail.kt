package noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_amount_payment.FragmentChangeAmountPayment
import noty_team.com.masterovik.ui.fragments.request.in_work.request_in_detail.change_deadlines.FragmentChangeDeadlines

class FragmentInDetail : BaseFragment<InDetailPresenter>() {


    companion object {

        fun newInstance() = FragmentInDetail()
    }

    override fun layout() = R.layout.fragment_request_order

    override fun initialization(view: View, isFirstInit: Boolean) {


        baseActivity.findViewById<View>(R.id.change_deadline).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentChangeDeadlines.newInstance(), null, true)

        }
        baseActivity.findViewById<View>(R.id.change_sum_request).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentChangeAmountPayment.newInstance(), null, true)

        }

        baseActivity.findViewById<View>(R.id.cross_fragment_in_details).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }


    }
}