package noty_team.com.masterovik.ui.fragments.request.canceled

import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

class CanceledRequestPresenter:BasePresenter(), CanceledRequestContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {

    }

    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }
}