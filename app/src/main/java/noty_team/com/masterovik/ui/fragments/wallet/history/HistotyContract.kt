package noty_team.com.masterovik.ui.fragments.wallet.history

import io.reactivex.Observable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface HistotyContract {
    interface View {

       // fun sortingHistoryAction():Observable<Any>
    }
    interface Presenter {
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}