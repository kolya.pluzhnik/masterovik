package noty_team.com.masterovik.ui.adapters.items_adapter

data class CanceledRequestItem(var nameOrder: String, var dateOrder: String, var price: Int, var description: String)