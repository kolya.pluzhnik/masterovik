package noty_team.com.masterovik.ui.fragments.orders.done_order.order

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.fragments.orders.complaint.FragmentComplaint

class FragmentDoneItem : BaseFragment<DoneItemPresenter>() {
    companion object {
        fun newInstance() = FragmentDoneItem()
    }

    override fun layout() = R.layout.fragment_done_item

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_done_item).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }

        baseActivity.findViewById<View>(R.id.leave_complaint_done).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentComplaint.newInstance(), null, true)
        }

    }
}