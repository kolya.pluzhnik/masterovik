package noty_team.com.masterovik.ui.fragments.advert.my_advert

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.adapters.items_adapter.CatalogAdvertItem
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.ui.adapters.recycler.AdvertAdapter
import noty_team.com.masterovik.ui.adapters.recycler.ProvideSeviceAdapter

class MyAdvertPresenter : BasePresenter(), MyAdvertContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()

        var recyclerMyAdvert = baseActivity.findViewById<RecyclerView>(R.id.recycler_my_advert)
        var recyclerProvideService = baseActivity.findViewById<RecyclerView>(R.id.recycler_provide_service)

        recyclerMyAdvert.layoutManager = LinearLayoutManager(baseActivity)
        recyclerMyAdvert.adapter = AdvertAdapter(createItemMyAdvert(), baseActivity)

        recyclerProvideService.layoutManager = GridLayoutManager(baseActivity, 3)
        recyclerProvideService.adapter = ProvideSeviceAdapter(createItemProvideService(), baseActivity)

    }


    override fun setupAction() {
    }

    private fun createItemMyAdvert(): ArrayList<CatalogAdvertItem> {

        var items = ArrayList<CatalogAdvertItem>()

        var item1 = CatalogAdvertItem("Название объявления", "23 февраля 2019", 20000, baseActivity.getString(R.string.some_text))

        items.add(item1)
        items.add(item1)
        items.add(item1)

        return items

    }

    private fun createItemProvideService(): ArrayList<ProvideServiceItem> {

        var items = ArrayList<ProvideServiceItem>()

        var item1 = ProvideServiceItem("Название услуги")

        items.add(item1)
        items.add(item1)
        items.add(item1)
        items.add(item1)
        items.add(item1)

        return items

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()

    }

    override fun stop() {

    }

    private fun initToolbar() {

    }
}