package noty_team.com.masterovik.ui.fragments.orders.current_order.order.change_date_order

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentChangeDateOrder : BaseFragment<ChangeDateOrderPresenter>() {

    companion object {
        fun newInstance() = FragmentChangeDateOrder()
    }

    override fun layout() = R.layout.fragment_change_date_order

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.findViewById<View>(R.id.cross_change_date_order).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }
}