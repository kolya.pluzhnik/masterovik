package noty_team.com.masterovik.ui.fragments.orders

import android.support.constraint.ConstraintLayout
import android.support.v4.widget.DrawerLayout
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.toolbar.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.R.id.filter
import noty_team.com.masterovik.R.id.right_container
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.orders.filter_order.FilterFragment

class OrderPresenter : OrderContract.Presenter, BasePresenter() {
    lateinit var baseActivity: BaseActivity


    override fun setupView() {
        initActionBar()
    }


    override fun setupAction() {

        baseActivity.findViewById<View>(R.id.filter)?.setOnClickListener {
            baseActivity.navigator.navigateToFragment(FilterFragment.newInstance(), null, true)
            baseActivity.getActionBarView()?.showAB(false)
            baseActivity.resizeContainerSmall()

        }
    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity

        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initActionBar() {


        baseActivity.getActionBarView()?.setupCenterText("Заказы")

    }

}