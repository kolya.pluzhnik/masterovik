package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.ui.adapters.items_adapter.CatalogAdvertItem
import noty_team.com.masterovik.ui.fragments.advert.advert_instance.FragmentInstanceAdvert

class AdvertAdapter(val items: ArrayList<CatalogAdvertItem>, val context: Context) :
        RecyclerView.Adapter<AdvertAdapter.ViewHolder>(), View.OnClickListener {

    var baseActivity = context as BaseActivity

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_catalog_advert, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.name.text = items.get(position).name
        holder?.date.text = items.get(position).date
        holder?.price.text = items.get(position).price.toString()
        holder?.description.text = items.get(position).description

        holder?.itemAction.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.item_catalog_advert -> {
                baseActivity.navigator.navigateToFragment(FragmentInstanceAdvert.newInstance(), null, true)
                baseActivity.resizeContainerSmall()
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var name = view.findViewById<TextView>(R.id.name_catalog_advert)
        var date = view.findViewById<TextView>(R.id.date_catalog_advert)
        var price = view.findViewById<TextView>(R.id.price_catalog_advert)
        var description = view.findViewById<TextView>(R.id.description_catalog_advert)


        var itemAction = view.findViewById<View>(R.id.item_catalog_advert)

    }
}