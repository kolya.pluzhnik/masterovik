package noty_team.com.masterovik.ui.fragments.advert.my_advert

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface MyAdvertContract {
    interface View
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}