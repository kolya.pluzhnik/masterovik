package noty_team.com.masterovik.ui.fragments.advert.advert_instance

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface InstanceAdvertContract{
    interface View{}
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }

}