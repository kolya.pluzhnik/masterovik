package noty_team.com.masterovik.ui.fragments.wallet.partners

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.adapters.items_adapter.PartnerItem
import noty_team.com.masterovik.ui.adapters.recycler.PartnerAdapter
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.FragmentSortingHistory

class ParthnersPresenter : BasePresenter(), PartnersContract.Presenter {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        var recyclerPartner = baseActivity.findViewById<RecyclerView>(R.id.recycler_partner)

        recyclerPartner.layoutManager = LinearLayoutManager(baseActivity)
        recyclerPartner.adapter = PartnerAdapter(createList(), baseActivity)

    }

    fun createList():ArrayList<PartnerItem>{
        var items = ArrayList<PartnerItem>()

        var partnerItem = PartnerItem("Имя Фамилия", "12 марта 2018", true)
        var partnerItem2 = PartnerItem("Имя Фамилия", "15 марта 2018", false)
        var partnerItem3 = PartnerItem("Имя Фамилия", "18 марта 2018", false)
        var partnerItem4 = PartnerItem("Имя Фамилия", "19 марта 2018", true)

        items.add(partnerItem)
        items.add(partnerItem2)
        items.add(partnerItem3)
        items.add(partnerItem4)

        return items

    }

    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
    }

    private fun initToolbar() {

    }
}