package noty_team.com.masterovik.ui.fragments.login.entrance

import android.os.CountDownTimer
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.login.selection_role.SelectionRoleFragment

class EntrancePresenter(var entranceView: EntranceContract.View) : EntranceContract.Presenter, BasePresenter() {

    lateinit var baseActivity: BaseActivity
    var timer = object : CountDownTimer(30000, 1000) {
        override fun onFinish() {
            entranceView.timerVisibility(false)
            entranceView.stateEnabled(true)
        }

        override fun onTick(millisUntilFinished: Long) {
            entranceView.setTime((millisUntilFinished / 1000).toString())
            entranceView.showSMSCode(true)
        }
    }

    override fun setupView() {
    }

    override fun setupAction() {
        setClickAction(entranceView.getCodeAction()) {
            timer.start()
            entranceView.stateEnabled(false)
        }


        setClickAction(entranceView.continueAction()) {
            baseActivity.navigator.navigateToFragment(SelectionRoleFragment.newInstance(), null,true)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
        disposable.clear()
    }

    /*private fun setClickAction(observable: Observable<Any>, onClick: () -> Unit) {
        disposable.add(observable
                .subscribe({
                    onClick()
                }, {

                }))
    }*/
}