package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.create_service

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentCreateService : BaseFragment<CreateServicePresenter>() {

    companion object {

        fun newInstance() = FragmentCreateService()
    }
    override fun layout() = R.layout.fragment_create_service

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.findViewById<View>(R.id.cross_create_service).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }
}