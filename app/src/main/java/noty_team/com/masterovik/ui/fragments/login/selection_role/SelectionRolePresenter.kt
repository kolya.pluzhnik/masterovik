package noty_team.com.masterovik.ui.fragments.login.selection_role

import android.os.Bundle
import android.widget.Toast
import io.reactivex.Observable
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.login.registration.RegistrationFragment
import noty_team.com.masterovik.utils.paper.PaperIO

class SelectionRolePresenter(var selectionRoleView: SelectionRoleContract.View) : SelectionRoleContract.Presenter, BasePresenter() {

    lateinit var baseActivity: BaseActivity
    companion object {
        var userRole = 0
    }


    override fun setupView() {
        selectionRoleView.setTextContinueButton("Продолжить")
        initActionBar()
    }

    override fun setupAction() {


        setClickAction(selectionRoleView.continueAction()) {


            when (userRole) {
                0 -> Toast.makeText(baseActivity.context, "Выберите роль", Toast.LENGTH_SHORT).show()
                1 -> {
                    /* //give service*/
                    PaperIO.setRole(false)
                    baseActivity.navigator.navigateToFragment(RegistrationFragment.newInstance(), null, true)
                }
                2 -> {
                    //use service
                    PaperIO.setRole(true)
                    baseActivity.navigator.navigateToFragment(RegistrationFragment.newInstance(), null, true)
                }
            }
        }

        setClickAction(baseActivity.getActionBarView()!!.leftButtonAction()) {
            baseActivity.navigationBackManager.navigateBack()
        }

        setClickAction(selectionRoleView.provideServicesAction()) {
            userRole = 1
            selectionRoleView.showProvideServicesText(true)
        }
        setClickAction(selectionRoleView.useServicesAction()) {
            userRole = 2
            selectionRoleView.showUseServicesText(true)
        }
    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    /*private fun setClickAction(observable: Observable<Any>, onClick: () -> Unit) {
        disposable.add(observable
                .subscribe({
                    onClick()
                }, {

                }))
    }*/

    private fun initActionBar() {
        baseActivity.getActionBarView()?.showAB(true)
        baseActivity.getActionBarView()?.changeSizeActionBar(true)
        baseActivity.getActionBarView()?.setupCenterText(baseActivity.resources.getString(R.string.registration))
    }
}