package noty_team.com.masterovik.ui.fragments.login.terms_agreement

import io.reactivex.Observable
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.activitys.main.MainActivity

class TermsPresenter(var termsView: TermsContract.View) : TermsContract.Presenter, BasePresenter() {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        termsView.countinueButtonText("Принять и продолжить")
        initActionBar()
    }

    override fun setupAction() {
        setClickAction(baseActivity.getActionBarView()!!.leftButtonAction()) {
            baseActivity.navigationBackManager.navigateBack()
        }

        setClickAction(termsView.continueAction()) {
            MainActivity.start(baseActivity)
        }
    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
        disposable.clear()
        disposable.dispose()
    }


    private fun initActionBar() {
        baseActivity.getActionBarView()!!.setupLeftButton(baseActivity.layoutInflater.inflate(R.layout.ab_back, null))
        baseActivity.getActionBarView()!!.showLeftButton(true)
    }
}