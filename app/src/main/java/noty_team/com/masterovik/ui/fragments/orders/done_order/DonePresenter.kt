package noty_team.com.masterovik.ui.fragments.orders.done_order

import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

class DonePresenter(var doneView: DoneContract.View): DoneContract.Presenter, BasePresenter() {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {

    }

    override fun setupAction() {
    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }
}