package noty_team.com.masterovik.ui.fragments.login.selection_role

import io.reactivex.Observable
import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface SelectionRoleContract {
    interface View{
        fun continueAction(): Observable<Any>

        fun provideServicesAction():Observable<Any>
        fun setTextContinueButton(text: String)

        fun useServicesAction():Observable<Any>

        fun showProvideServicesText(show: Boolean)

        fun showUseServicesText(show: Boolean)
    }

    interface Presenter{

        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()

    }
}