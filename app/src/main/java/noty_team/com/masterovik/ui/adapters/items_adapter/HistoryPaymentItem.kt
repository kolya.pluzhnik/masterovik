package noty_team.com.masterovik.ui.adapters.items_adapter

data class HistoryPaymentItem(var isItemParameter : Boolean, var dataPayment: String, var balance: Int, var amount: Int)