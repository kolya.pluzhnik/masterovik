package noty_team.com.masterovik.ui.fragments.login.entrance

import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_entrance.*
import noty_team.com.masterovik.R

class EntranceView(override val containerView: View?) : LayoutContainer, EntranceContract.View {

    override fun showSMSCode(show: Boolean) {
        if (show) {
            get_key.text = containerView?.context?.resources?.getString(R.string.send_code_again).toString()
            sms_code.visibility = View.VISIBLE
        } else {
            get_key.text = containerView?.context?.resources?.getString(R.string.get_code).toString()
            sms_code.visibility = View.GONE
        }
    }


    override fun getCodeAction(): Observable<Any> {
        return RxView.clicks(get_key)
    }

    override fun continueAction(): Observable<Any> {
        return RxView.clicks(continue_btn_entrance)
    }


    override fun setTime(time: String) {
        stateEnabled(false)
        timerVisibility(true)
        timer_text.text = time
    }

    override fun timerVisibility(show: Boolean) {
        if (show) timer_text.visibility = View.VISIBLE
        else timer_text.visibility = View.GONE
    }

    override fun stateEnabled(stateEnabled: Boolean) {
        get_key.isClickable = stateEnabled
    }


}
