package noty_team.com.masterovik.ui.fragments.login.registration

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_registration_form.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment


class RegistrationFragment : BaseFragment<RegistrationPresenter>() {
    lateinit var registrationView: RegistrationContract.View
    lateinit var registrationPresenter: RegistrationContract.Presenter

    companion object {
        fun newInstance(): RegistrationFragment {
            return RegistrationFragment()
        }
    }


    override fun layout() = R.layout.fragment_registration_form

    override fun initialization(view: View, isFirstInit: Boolean) {

        registrationView = RegistrationView(fragment_registration)
        registrationPresenter = RegistrationPresenter(registrationView)

    }

    override fun onStart() {
        super.onStart()
        registrationPresenter.start(baseActivity)
    }

    override fun onStop() {
        super.onStop()
        registrationPresenter.stop()
    }
}