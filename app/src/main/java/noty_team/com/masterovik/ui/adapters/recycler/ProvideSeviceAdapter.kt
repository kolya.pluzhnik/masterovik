package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem

class ProvideSeviceAdapter (val items: ArrayList<ProvideServiceItem>, val context: Context) :
        RecyclerView.Adapter<ProvideSeviceAdapter.ViewHolder>(), View.OnClickListener {

    var baseActivity = context as BaseActivity

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_provide_service, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.nameProvideSevie.text = items.get(position).nameProvideService


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.open_request_order -> {

            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameProvideSevie = view.findViewById<TextView>(R.id.name_provide_service)


    }
}