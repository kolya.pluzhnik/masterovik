package noty_team.com.masterovik.ui.fragments.advert.catalog_advert.filter_advert

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface FilterAdvertContract {

    interface View
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}