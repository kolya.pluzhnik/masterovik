package noty_team.com.masterovik.ui.fragments.faq

import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

class FAQBasePresenter(var faqBaseView: FAQBaseContract.View) : BasePresenter(), FAQBaseContract.Presenter {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {

    }

    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }
}