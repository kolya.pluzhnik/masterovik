package noty_team.com.masterovik.ui.fragments.login.terms_agreement

import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.button_continue.*

class TermsView(override val containerView: View?) : LayoutContainer, TermsContract.View {
    override fun continueAction(): Observable<Any> {
        return RxView.clicks(continue_btn)
    }

    override fun countinueButtonText(text: String) {
        continue_btn.text = text
    }
}