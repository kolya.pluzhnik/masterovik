package noty_team.com.masterovik.ui.fragments.advert.catalog_advert

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.adapters.items_adapter.CatalogAdvertItem
import noty_team.com.masterovik.ui.adapters.recycler.AdvertAdapter

class CatalogAdvertPresenter : BasePresenter(), CatalogAdvertContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
        var recyclerCatalog = baseActivity.findViewById<RecyclerView>(R.id.recycler_catalog_advert)
        recyclerCatalog.layoutManager = LinearLayoutManager(baseActivity)
        recyclerCatalog.adapter = AdvertAdapter(createItem(), baseActivity)
    }


    override fun setupAction() {


    }

    fun createItem(): ArrayList<CatalogAdvertItem> {

        var items = ArrayList<CatalogAdvertItem>()

        var item1 = CatalogAdvertItem("Название объявления", "23 февраля 2019", 20000, baseActivity.getString(R.string.some_text))

        items.add(item1)
        items.add(item1)
        items.add(item1)

        return items

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initToolbar() {

    }
}