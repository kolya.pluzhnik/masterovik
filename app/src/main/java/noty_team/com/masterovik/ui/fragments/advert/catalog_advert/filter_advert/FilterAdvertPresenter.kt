package noty_team.com.masterovik.ui.fragments.advert.catalog_advert.filter_advert

import android.support.constraint.ConstraintLayout
import android.view.View
import android.widget.FrameLayout
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter


class FilterAdvertPresenter(var filterAdvertView: FilterAdvertContract.View) : BasePresenter(), FilterAdvertContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
    }


    override fun setupAction() {
        baseActivity.findViewById<View>(R.id.cross_filter_advert).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initToolbar() {

    }
}