package noty_team.com.masterovik.ui.adapters.items_adapter

data class CanceledOrderItem (var nameOrder: String = "Название заказа", var nameSurname: String = "Имя Фамилия",
                              var day:Int, var canceledInfo: String)