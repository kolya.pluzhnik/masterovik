package noty_team.com.masterovik.ui.fragments.request.in_work

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_in_work_request.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.InWorkRequestItem
import noty_team.com.masterovik.ui.adapters.recycler.InWorkRequestAdapter

class FragmentInWorkRequest : BaseFragment<InWorkRequestPresenter>() {

    lateinit var inWorkRequestView: InWorkRequestContract.View
    lateinit var inWorkRequestPresenter: InWorkRequestContract.Presenter

    override fun layout() = R.layout.fragment_in_work_request

    override fun initialization(view: View, isFirstInit: Boolean) {

        inWorkRequestView = InWorkRequestView()
        inWorkRequestPresenter = InWorkRequestPresenter()


        recycler_in_work_request.layoutManager = LinearLayoutManager(baseActivity)
        recycler_in_work_request.adapter = InWorkRequestAdapter(createList(), baseActivity)

    }

    fun createList(): ArrayList<InWorkRequestItem> {

        var list: ArrayList<InWorkRequestItem> = ArrayList()
        var item = InWorkRequestItem("Название объявления", "23 февраля 2019", 20000, getString(R.string.some_text))

        list.add(item)
        list.add(item)
        list.add(item)
        list.add(item)


        return list

    }


    override fun onStart() {
        super.onStart()
        inWorkRequestPresenter.start(baseActivity)
    }
}