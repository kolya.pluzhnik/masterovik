package noty_team.com.masterovik.ui.fragments.wallet.sorting_history

import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_sorting_history.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.fragments.wallet.BaseWalletPresenter

class FragmentSortingHistory : BaseFragment<SortingHistoryPresenter>() {
    lateinit var sortingHistoryView: SortingHistoryContract.View
    lateinit var sortingHistoryPresenter: SortingHistoryContract.Presenter

    companion object {
        fun newInstance() = FragmentSortingHistory()
    }

    override fun layout() = R.layout.fragment_sorting_history

    override fun initialization(view: View, isFirstInit: Boolean) {
        sortingHistoryView = SortingHistoryView(fragment_sorting_history)
        sortingHistoryPresenter = SortingHistoryPresenter(sortingHistoryView)
    }

    override fun onStart() {
        super.onStart()
        sortingHistoryPresenter.start(baseActivity)
    }

    override fun onDestroyView() {


        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        super.onDestroyView()
    }
}