package noty_team.com.masterovik.ui.fragments.orders.canceled_order

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_canceled.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.items_adapter.CanceledOrderItem
import noty_team.com.masterovik.ui.adapters.items_adapter.CurrentOrderItem
import noty_team.com.masterovik.ui.adapters.recycler.CanceledOrderAdapter

class CanceledFragment : BaseFragment<CanceledPresenter>() {

    lateinit var canceledView: CanceledContract.View
    lateinit var canceledPresenter: CanceledContract.Presenter

    companion object {
        fun newInstance(): CanceledFragment {
            return CanceledFragment()
        }
    }

    override fun layout() = R.layout.fragment_canceled

    override fun initialization(view: View, isFirstInit: Boolean) {
        canceledView = CanceledView(fragment_canceled)
        canceledPresenter = CanceledPresenter(canceledView)

        recycler_canceled_order.layoutManager = LinearLayoutManager(baseActivity)
        recycler_canceled_order.adapter = CanceledOrderAdapter(createList(), baseActivity)
    }

    private fun createList(): ArrayList<CanceledOrderItem> {
        var list: ArrayList<CanceledOrderItem> = ArrayList()
        var item1 = CanceledOrderItem("Название Заказа", "Имя Фамилия", 15, "Отменено заказчиком")
        var item2 = CanceledOrderItem("Название Заказа", "Имя Фамилия", 22, "Отменено вами")

        list.add(item1)
        list.add(item2)
        return list
    }

    override fun onStart() {
        super.onStart()
        canceledPresenter.start(baseActivity)
    }
}