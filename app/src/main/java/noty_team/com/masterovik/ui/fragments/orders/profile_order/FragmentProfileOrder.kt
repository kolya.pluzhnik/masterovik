package noty_team.com.masterovik.ui.fragments.orders.profile_order

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentProfileOrder : BaseFragment<ProfilePresenterOrder>() {

    companion object {
        fun newInstance() = FragmentProfileOrder()
    }

    override fun layout() = R.layout.fragment_profile_order

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_profile).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }

    }
}