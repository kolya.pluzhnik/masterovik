package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.adapters.items_adapter.ProvideServiceItem
import noty_team.com.masterovik.ui.adapters.recycler.ProvideSeviceAdapter
import noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice.create_service.FragmentCreateService

class EditSettingServicePresenter : BasePresenter(), EditSettingServiceContract.Presenter {

    lateinit var baseActivity: BaseActivity

    override fun setupView() {

        var recyclerProvideService = baseActivity.findViewById<RecyclerView>(R.id.recycler_setting_provide_service)

        recyclerProvideService.layoutManager = GridLayoutManager(baseActivity, 3)
        recyclerProvideService.adapter = ProvideSeviceAdapter(createItemProvideService(), baseActivity)

    }

    private fun createItemProvideService(): ArrayList<ProvideServiceItem> {

        var items = ArrayList<ProvideServiceItem>()

        var item1 = ProvideServiceItem("Название услуги")

        items.add(item1)
        items.add(item1)
        items.add(item1)
        items.add(item1)
        items.add(item1)

        return items

    }

    override fun setupAction() {

        baseActivity.findViewById<View>(R.id.cross_setting_service).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
        baseActivity.findViewById<View>(R.id.create_service).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentCreateService.newInstance(), null, true)
        }
    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
    }
}