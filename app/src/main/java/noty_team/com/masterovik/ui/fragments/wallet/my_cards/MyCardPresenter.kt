package noty_team.com.masterovik.ui.fragments.wallet.my_cards

import android.view.LayoutInflater
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.FragmentSortingHistory
import noty_team.com.masterovik.ui.fragments.wallet.top_up_balance.FragmentTopUpBalance
import noty_team.com.masterovik.ui.fragments.wallet.top_up_balance.TopUpBalanceContract

class MyCardPresenter(var myCardView: MyCardsContract.View) : BasePresenter(), MyCardsContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
    }

    override fun setupAction() {

        setClickAction(myCardView.topUpBalanceAction()) {
            baseActivity.navigator.navigateToFragment(FragmentTopUpBalance.newInstance(), null, true)

            baseActivity.resizeContainerSmall()
        }

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
    }

    private fun initToolbar() {

    }
}