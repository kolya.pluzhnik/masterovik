package noty_team.com.masterovik.ui.fragments.wallet.my_cards

import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_my_cards.*

class MyCardView(override val containerView: View?) : LayoutContainer, MyCardsContract.View {

    override fun topUpBalanceAction(): Observable<Any> {
        return RxView.clicks(top_up_balance_btn)
    }
}