package noty_team.com.masterovik.ui.fragments.advert.advert_instance

import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.orders.profile_order.FragmentProfileOrder

class InstanceAdvertPresenter : BasePresenter(), InstanceAdvertContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
    }


    override fun setupAction() {

        baseActivity.findViewById<FrameLayout>(R.id.cross_instance_advert).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
        baseActivity.findViewById<View>(R.id.user_name).setOnClickListener {
            baseActivity.navigator.navigateToFragment(FragmentProfileOrder.newInstance(), null, true)
        }


    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initToolbar() {
    }
}