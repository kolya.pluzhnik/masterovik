package noty_team.com.masterovik.ui.fragments.faq

import android.view.View
import kotlinx.android.extensions.LayoutContainer

class FAQBaseView(override val containerView: View?) : LayoutContainer, FAQBaseContract.View {
}