package noty_team.com.masterovik.ui.fragments.profile_setting.edit_phone_number

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentEditPhoneNumber : BaseFragment<EditPhoneNumberPresenter>() {

    companion object {
        fun newInstance() = FragmentEditPhoneNumber()
    }
    override fun layout() = R.layout.fragment_edit_phone_number

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_edit_phone_number).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }
}