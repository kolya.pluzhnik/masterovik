package noty_team.com.masterovik.ui.fragments.wallet.top_up_balance

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface TopUpBalanceContract {
    interface View{}
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}