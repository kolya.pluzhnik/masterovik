package noty_team.com.masterovik.ui.fragments.request

import android.support.design.widget.TabLayout
import android.view.View
import kotlinx.android.synthetic.main.fragment_base_request.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.view_pager.RequestPageAdapter
import noty_team.com.masterovik.ui.fragments.orders.filter_order.FilterFragment
import noty_team.com.masterovik.ui.fragments.request.canceled.FragmentCanceledRequest
import noty_team.com.masterovik.ui.fragments.request.in_work.FragmentInWorkRequest

class FragmentBaseRequest : BaseFragment<BaseRequestPresenter>() {
    lateinit var baseRequestPresenter: BaseRequestContract.Presenter
    lateinit var baseRequestView: BaseRequestContract.View

    companion object {
        fun newInstance() = FragmentBaseRequest()
    }


    override fun layout() = R.layout.fragment_base_request

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseRequestView = BaseRequestView()
        baseRequestPresenter = BaseRequestPresenter()


        view_pager_request.adapter = RequestPageAdapter(childFragmentManager, view_pager_request,
                arrayListOf(FragmentInWorkRequest(), FragmentCanceledRequest()))
        tabLayout_request.setupWithViewPager(view_pager_request)

        tabLayout_request.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {

                    baseActivity.findViewById<View>(R.id.filter).visibility = View.VISIBLE

                } else {
                    baseActivity.findViewById<View>(R.id.filter).visibility = View.GONE
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


    }

    override fun onStart() {
        super.onStart()
        baseRequestPresenter.start(baseActivity)
    }
}