package noty_team.com.masterovik.ui.adapters.items_adapter

data class CatalogAdvertItem(var name:String, var date: String, var price:Int, var description: String)