package noty_team.com.masterovik.ui.activitys.login

import android.content.Intent
import kotlinx.android.synthetic.main.activity_login.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.base_action_bar.ActionBarContract
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarPresenter
import noty_team.com.masterovik.base.base_action_bar.BaseActionBarView
import noty_team.com.masterovik.ui.fragments.login.entrance.EntranceFragment

class LoginActivity : BaseActivity() {
    private lateinit var actionBarView: ActionBarContract.View
    private lateinit var actionBarPresenter: ActionBarContract.Presenter

    companion object {
        lateinit var baseActivity: BaseActivity

        fun start(baseActivity: BaseActivity) {
            this.baseActivity = baseActivity
            baseActivity.startActivity(Intent(baseActivity, LoginActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        }
    }

    override fun layout(): Int = R.layout.activity_login

    override fun initialization() {

        navigator.navigateToFragment(EntranceFragment.newInstance(), null,false)

        actionBarView = BaseActionBarView(toolbar_login, activity_login, this)
        actionBarPresenter = BaseActionBarPresenter(actionBarView, baseActivity)
    }

    override fun onStart() {
        super.onStart()
        actionBarPresenter.start()
    }

    override fun onStop() {
        super.onStop()
        actionBarPresenter.stop()
    }

    override fun getActionBarView(): ActionBarContract.View? = actionBarView
}
