package noty_team.com.masterovik.ui.fragments.profile_setting.edit_setting_sevice

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentEditSettingService : BaseFragment<EditSettingServicePresenter>() {

    lateinit var editSettingServicePresenter: EditSettingServiceContract.Presenter

    companion object {
        fun newInstance() = FragmentEditSettingService()
    }

    override fun layout() = R.layout.fragment_setting_service

    override fun initialization(view: View, isFirstInit: Boolean) {

        editSettingServicePresenter = EditSettingServicePresenter()


    }

    override fun onStart() {
        super.onStart()
        editSettingServicePresenter.start(baseActivity)
    }

    override fun onStop() {
        super.onStop()
    }
}