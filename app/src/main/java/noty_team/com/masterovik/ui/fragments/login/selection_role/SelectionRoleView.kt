package noty_team.com.masterovik.ui.fragments.login.selection_role

import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.button_continue.*
import kotlinx.android.synthetic.main.fragment_selectino_role.*


class SelectionRoleView(override val containerView: View?) : LayoutContainer, SelectionRoleContract.View {


    override fun continueAction(): Observable<Any> {
        return RxView.clicks(continue_btn)
    }

    override fun provideServicesAction(): Observable<Any> {
        return RxView.clicks(provide_services)
    }

    override fun useServicesAction(): Observable<Any> {
        return RxView.clicks(use_services)
    }

    override fun showProvideServicesText(show: Boolean) {
        if (show) {
            provide_services_text.visibility = View.VISIBLE
            showUseServicesText(!show)
        } else {
            provide_services_text.visibility = View.GONE
        }

    }

    override fun showUseServicesText(show: Boolean) {
        if (show) {
            use_services_text.visibility = View.VISIBLE
            showProvideServicesText(!show)
        } else {
            use_services_text.visibility = View.GONE
        }
    }

    override fun setTextContinueButton(text: String) {
        continue_btn.text = text
    }
}