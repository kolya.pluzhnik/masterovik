package noty_team.com.masterovik.ui.fragments.orders.canceled_order

import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter

class CanceledPresenter(var canceledView: CanceledContract.View) : CanceledContract.Presenter, BasePresenter() {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {

    }

    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {
    }
}