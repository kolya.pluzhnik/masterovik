package noty_team.com.masterovik.ui.fragments.orders

import noty_team.com.masterovik.base.mvp.activity.BaseActivity

interface OrderContract {
    interface View{

    }
    interface Presenter{
        fun setupView()

        fun setupAction()

        fun start(baseActivity: BaseActivity)

        fun stop()
    }
}