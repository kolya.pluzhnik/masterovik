package noty_team.com.masterovik.ui.adapters.recycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.ui.adapters.items_adapter.PartnerItem

class PartnerAdapter(val items: ArrayList<PartnerItem>, val context: Context) :
        RecyclerView.Adapter<PartnerAdapter.ViewHolder>(), View.OnClickListener {

    var baseActivity = context as BaseActivity

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_parthner, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.userName.text = items.get(position).partnerName
        holder?.date.text = items.get(position).date


        if (items.get(position).isActivated) {
            holder?.bottomContainer.background = baseActivity.getDrawable(R.drawable.background_items_in_work)
            holder?.bottonText.text = baseActivity.getString(R.string.partner_activated)
            holder?.bottonText.setTextColor(baseActivity.resources.getColor(R.color.colorGreen))
        } else {

            holder?.bottomContainer.background = baseActivity.getDrawable(R.drawable.background_gray_bottom_partner)
            holder?.bottonText.text = baseActivity.getString(R.string.partner_not_activated)
            holder?.bottonText.setTextColor(baseActivity.resources.getColor(R.color.colorWhite))
        }


    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var userName = view.findViewById<TextView>(R.id.partner_user_name)
        var date = view.findViewById<TextView>(R.id.partner_date)
        var bottomContainer = view.findViewById<View>(R.id.bottom_hint)
        var bottonText = view.findViewById<TextView>(R.id.activated_info)
    }
}