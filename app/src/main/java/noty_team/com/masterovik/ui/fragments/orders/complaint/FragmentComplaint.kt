package noty_team.com.masterovik.ui.fragments.orders.complaint

import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment

class FragmentComplaint : BaseFragment<ComplainPresenter>() {

    companion object {
        fun newInstance() = FragmentComplaint()
    }

    override fun layout() = R.layout.fragment_complaint

    override fun initialization(view: View, isFirstInit: Boolean) {

        baseActivity.findViewById<View>(R.id.cross_complaint).setOnClickListener {
            baseActivity.navigationBackManager.navigateBack()
        }
    }
}