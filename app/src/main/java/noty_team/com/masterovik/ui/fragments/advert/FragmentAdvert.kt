package noty_team.com.masterovik.ui.fragments.advert

import android.support.constraint.ConstraintLayout
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.fragment_base_advert.*
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.fragment.BaseFragment
import noty_team.com.masterovik.ui.adapters.view_pager.AdvertPageAdapter
import noty_team.com.masterovik.ui.fragments.advert.catalog_advert.FragmentCatalogAdvert
import noty_team.com.masterovik.ui.fragments.advert.catalog_advert.filter_advert.FragmentFilterAdvert
import noty_team.com.masterovik.ui.fragments.advert.my_advert.FragmentMyAdvert
import noty_team.com.masterovik.ui.fragments.wallet.sorting_history.FragmentSortingHistory

class FragmentAdvert : BaseFragment<AdvertPresenter>() {

    lateinit var advertView: AdvertContract.View
    lateinit var advertPresenter: AdvertContract.Presenter

    companion object {
        fun newInstance(): FragmentAdvert {
            return FragmentAdvert()
        }
    }

    override fun layout() = R.layout.fragment_base_advert

    override fun initialization(view: View, isFirstInit: Boolean) {

        advertView = AdvertView()
        advertPresenter = AdvertPresenter()


        view_pager_advert.adapter = AdvertPageAdapter(childFragmentManager, view_pager_advert,
                arrayListOf(FragmentMyAdvert(), FragmentCatalogAdvert()))
        tabLayout_advert.setupWithViewPager(view_pager_advert)

        tabLayout_advert.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    override fun onStart() {
        super.onStart()
        advertPresenter.start(baseActivity)
    }
}