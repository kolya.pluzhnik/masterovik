package noty_team.com.masterovik.ui.fragments.advert

import android.view.LayoutInflater
import android.view.View
import noty_team.com.masterovik.R
import noty_team.com.masterovik.base.mvp.activity.BaseActivity
import noty_team.com.masterovik.base.mvp.activity.BasePresenter
import noty_team.com.masterovik.ui.fragments.advert.catalog_advert.filter_advert.FragmentFilterAdvert

class AdvertPresenter : BasePresenter(), AdvertContract.Presenter {
    lateinit var baseActivity: BaseActivity

    override fun setupView() {
        initToolbar()
    }


    override fun setupAction() {

    }

    override fun start(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        setupView()
        setupAction()
    }

    override fun stop() {

    }

    private fun initToolbar() {


        baseActivity.getActionBarView()?.setSmallContainerWidth()
        baseActivity.getActionBarView()?.setLargeToolbarHeight()
        baseActivity.getActionBarView()?.setupCenterText("Объявления")
        baseActivity.getActionBarView()?.setupRightButton(LayoutInflater.from(baseActivity).inflate(R.layout.ab_right_filter, null))

        baseActivity.resizeContainerLarge()

        baseActivity.findViewById<View>(R.id.filter).setOnClickListener {

            baseActivity.navigator.navigateToFragment(FragmentFilterAdvert.newInstance(), null, true)
            baseActivity.resizeContainerSmall()
        }
    }
}
